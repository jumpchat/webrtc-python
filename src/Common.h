#ifndef WEBRTC_COMMON_H
#define WEBRTC_COMMON_H

#ifdef WEBRTC_WIN
#define __PRETTY_FUNCTION__ __FUNCSIG__
#include <Winsock2.h>
#endif

#include <Python.h>

#include "webrtc/base/atomicops.h"
#include "webrtc/base/basictypes.h"
#include "webrtc/base/buffer.h"
#include "webrtc/base/json.h"
#include "webrtc/base/logging.h"
#include "webrtc/base/refcount.h"
#include "webrtc/base/scoped_ref_ptr.h"
#include "webrtc/base/scoped_ref_ptr.h"
#include "webrtc/base/ssladapter.h"
#include "webrtc/base/sslstreamadapter.h"
#include "webrtc/base/stringencode.h"
#include "webrtc/base/stringutils.h"
#include "webrtc/base/thread.h"

#include "webrtc/api/datachannelinterface.h"
#include "webrtc/api/jsep.h"
#include "webrtc/api/jsepsessiondescription.h"
#include "webrtc/api/mediaconstraintsinterface.h"
#include "webrtc/api/mediastreaminterface.h"
#include "webrtc/api/peerconnectioninterface.h"
#include "webrtc/api/test/fakeconstraints.h"
#include "webrtc/api/video/video_frame.h"
#include "webrtc/api/video/i420_buffer.h"
#include "webrtc/pc/peerconnectionfactory.h"

// #include "api/videosourceinterface.h"
#include "webrtc/media/base/videosourceinterface.h"
#include "webrtc/media/base/videosinkinterface.h"
#include "webrtc/media/engine/webrtcvideocapturerfactory.h"
#include "webrtc/modules/video_capture/video_capture_factory.h"

#if defined(WEBRTC_WIN)
#include "webrtc/base/win32socketinit.h"
#include "webrtc/base/win32socketserver.h"
#endif

#if (PY_MAJOR_VERSION == 2)
char *PyUnicode_AsUTF8(PyObject *str);
#endif

#endif