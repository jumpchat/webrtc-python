#include "PeerConnection.h"
#include "Common.h"
#include "MediaConstraints.h"
#include "MediaStream.h"
#include "Observer.h"
#include "Platform.h"
#include "Utils.h"

using namespace WebRTC;

PeerConnection::PeerConnection(PyObject* config, PyObject* constraints)
{
    LOG(LS_INFO) << __PRETTY_FUNCTION__;

    // create config
    webrtc::PeerConnectionInterface::RTCConfiguration _config;
    if (config && PyDict_Check(config)) {
        PyObject* servers = PyDict_GetItemString(config, "iceServers");
        if (servers && PyList_Check(servers)) {
            for (Py_ssize_t index = 0; index < PyList_Size(servers); index++) {
                PyObject* server = PyList_GetItem(servers, index);

                PyObject* url_value = PyDict_GetItemString(server, "url");
                PyObject* username_value = PyDict_GetItemString(server, "username");
                PyObject* credential_value = PyDict_GetItemString(server, "credential");

                webrtc::PeerConnectionInterface::IceServer entry;
                if (url_value && PyUnicode_Check(url_value)) {
                    entry.uri = PyUnicode_AsUTF8(url_value);

                    if (username_value && PyUnicode_Check(username_value)) {
                        entry.username = PyUnicode_AsUTF8(username_value);
                    }

                    if (credential_value && PyUnicode_Check(credential_value)) {
                        entry.password = PyUnicode_AsUTF8(credential_value);
                    }

                    _config.servers.push_back(entry);
                }
            }
        }
    }

    _observer = new PCObserver(this);

    // create constraints
    rtc::scoped_refptr<MediaConstraints> _constraints = MediaConstraints::New(constraints);

    _pc = Platform::GetFactory()->CreatePeerConnection(_config, _constraints, NULL, NULL, _observer);
}

PeerConnection::~PeerConnection()
{
    LOG(LS_INFO) << __PRETTY_FUNCTION__;

    if (_pc.get()) {
        webrtc::PeerConnectionInterface::SignalingState state(_pc->signaling_state());

        if (state != webrtc::PeerConnectionInterface::kClosed) {
            _pc->Close();
        }
    }
    _observer->SetPeerConnection(NULL);
}

bool PeerConnection::AddIceCandidate(PyObject* candidate)
{
    LOG(LS_INFO) << __PRETTY_FUNCTION__;

    if (candidate && PyDict_Check(candidate)) {
        std::string sdpMid;
        std::string sdpMLineIndex;
        std::string sdp;
        PyObject* sdpMid_obj = PyDict_GetItemString(candidate, "sdpMid");
        if (sdpMid_obj) {
            sdpMid = PyUnicode_AsUTF8(sdpMid_obj);
        }

        PyObject* sdpMLineIndex_obj = PyDict_GetItemString(candidate, "sdpMLineIndex");
        if (sdpMLineIndex_obj) {
            sdpMLineIndex = PyUnicode_AsUTF8(sdpMLineIndex_obj);
        }

        PyObject* sdp_obj = PyDict_GetItemString(candidate, "candidate");
        if (sdp_obj) {
            sdp = PyUnicode_AsUTF8(sdp_obj);
        }

        std::unique_ptr<webrtc::IceCandidateInterface> ice_candidate(webrtc::CreateIceCandidate(
            sdpMid,
            atoi(sdpMLineIndex.c_str()),
            sdp,
            0));
        return _pc->AddIceCandidate(ice_candidate.get());
    }

    return false;
}

void PeerConnection::AddStream(MediaStream* stream)
{
    LOG(LS_INFO) << __PRETTY_FUNCTION__;
    _pc->AddStream(stream->_stream);
}

void PeerConnection::RemoveStream(MediaStream* stream)
{
    LOG(LS_INFO) << __PRETTY_FUNCTION__;
    _pc->RemoveStream(stream->_stream);
}

void PeerConnection::CreateOffer(PyObject* constraints, PyObject* future)
{
    LOG(LS_INFO) << __PRETTY_FUNCTION__;
    rtc::scoped_refptr<MediaConstraints> _constraints = MediaConstraints::New(constraints);
    rtc::scoped_refptr<SDPObserver> observer = new SDPObserver(future);
    _pc->CreateOffer(observer, _constraints);
}

void PeerConnection::CreateAnswer(PyObject* constraints, PyObject* future)
{
    LOG(LS_INFO) << __PRETTY_FUNCTION__;
    rtc::scoped_refptr<MediaConstraints> _constraints = MediaConstraints::New(constraints);
    rtc::scoped_refptr<SDPObserver> observer = new SDPObserver(future);
    _pc->CreateAnswer(observer, _constraints);
}

void PeerConnection::SetLocalSessionDescription(PyObject *sdp, PyObject* future)
{
    LOG(LS_INFO) << __PRETTY_FUNCTION__;
    rtc::scoped_refptr<SetSDPObserver> observer = new SetSDPObserver(future);
    PyObject *type = PyDict_GetItemString(sdp, "type");
    PyObject *sdpObject = PyDict_GetItemString(sdp, "sdp");
    webrtc::SessionDescriptionInterface *sdi = webrtc::CreateSessionDescription(PyUnicode_AsUTF8(type), PyUnicode_AsUTF8(sdpObject), NULL);
    _pc->SetLocalDescription(observer, sdi);
}

void PeerConnection::SetRemoteSessionDescription(PyObject *sdp, PyObject* future)
{
    LOG(LS_INFO) << __PRETTY_FUNCTION__;
    rtc::scoped_refptr<SetSDPObserver> observer = new SetSDPObserver(future);
    PyObject *type = PyDict_GetItemString(sdp, "type");
    PyObject *sdpObject = PyDict_GetItemString(sdp, "sdp");
    webrtc::SessionDescriptionInterface *sdi = webrtc::CreateSessionDescription(PyUnicode_AsUTF8(type), PyUnicode_AsUTF8(sdpObject), NULL);
    _pc->SetRemoteDescription(observer, sdi);
}

void PeerConnection::Close()
{
    LOG(LS_INFO) << __PRETTY_FUNCTION__;
    _pc->Close();
}

void PeerConnection::SetObserver(PyObject *observer, PyObject *event_handler) {
    _observer->SetObserver(observer, event_handler);
}
