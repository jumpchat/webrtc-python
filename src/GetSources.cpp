#include "Common.h"
#include "GetSources.h"
#include "Platform.h"

using namespace WebRTC;

rtc::scoped_refptr<webrtc::AudioTrackInterface> GetSources::GetAudioSource(const rtc::scoped_refptr<MediaConstraints>& constraints) {
    LOG(LS_INFO) << __PRETTY_FUNCTION__;

    rtc::scoped_refptr<webrtc::AudioTrackInterface> track;
    rtc::scoped_refptr<webrtc::PeerConnectionFactoryInterface> factory = Platform::GetFactory();

    if (factory.get()) {
        track = factory->CreateAudioTrack("audio", factory->CreateAudioSource(constraints->ToConstraints()));
    }

    return track;
}

rtc::scoped_refptr<webrtc::AudioTrackInterface> GetSources::GetAudioSource(const std::string id, const rtc::scoped_refptr<MediaConstraints>& constraints) {
    LOG(LS_INFO) << __PRETTY_FUNCTION__;
    return GetSources::GetAudioSource(constraints);
}

rtc::scoped_refptr<webrtc::VideoTrackInterface> GetSources::GetVideoSource(const rtc::scoped_refptr<MediaConstraints>& constraints) {
    LOG(LS_INFO) << __PRETTY_FUNCTION__;

    cricket::VideoCapturer* capturer;
    rtc::scoped_refptr<webrtc::VideoTrackInterface> track;
    rtc::scoped_refptr<webrtc::PeerConnectionFactoryInterface> factory = Platform::GetFactory();
    std::unique_ptr<webrtc::VideoCaptureModule::DeviceInfo> video_info(webrtc::VideoCaptureFactory::CreateDeviceInfo());
    cricket::WebRtcVideoDeviceCapturerFactory device_factory;

    if (factory.get()) {
        if (video_info) {
            int num_devices = video_info->NumberOfDevices();

            for (int i = 0; i < num_devices; ++i) {
                const uint32_t kSize = 256;
                char name[kSize] = { 0 };
                char id[kSize] = { 0 };

                if (video_info->GetDeviceName(i, name, kSize, id, kSize) != -1) {
                    capturer = device_factory.Create(cricket::Device(name, 0));

                    if (capturer) {
                        track = factory->CreateVideoTrack("video", factory->CreateVideoSource(capturer, constraints->ToConstraints()));
                        return track;
                    }
                }
            }
        }
    }

    return track;
}

rtc::scoped_refptr<webrtc::VideoTrackInterface> GetSources::GetVideoSource(const std::string id_name, const rtc::scoped_refptr<MediaConstraints>& constraints) {
    LOG(LS_INFO) << __PRETTY_FUNCTION__;

    cricket::VideoCapturer* capturer;
    rtc::scoped_refptr<webrtc::VideoTrackInterface> track;
    rtc::scoped_refptr<webrtc::PeerConnectionFactoryInterface> factory = Platform::GetFactory();
    std::unique_ptr<webrtc::VideoCaptureModule::DeviceInfo> video_info(webrtc::VideoCaptureFactory::CreateDeviceInfo());
    cricket::WebRtcVideoDeviceCapturerFactory device_factory;

    if (factory.get()) {
        if (video_info) {
            int num_devices = video_info->NumberOfDevices();

            for (int i = 0; i < num_devices; ++i) {
                const uint32_t kSize = 256;
                char name[kSize] = { 0 };
                char id[kSize] = { 0 };

                if (video_info->GetDeviceName(i, name, kSize, id, kSize) != -1) {
                    if (id_name.empty() || id_name.compare(name) == 0) {
                        capturer = device_factory.Create(cricket::Device(name, 0));

                        if (capturer) {
                            track = factory->CreateVideoTrack("video", factory->CreateVideoSource(capturer, constraints->ToConstraints()));
                            return track;
                        }
                    }
                }
            }
        }
    }

    return track;
}

PyObject* GetSources::GetDevices() {
    LOG(LS_INFO) << __PRETTY_FUNCTION__;

    uint32_t index = 0;
    std::unique_ptr<webrtc::VideoCaptureModule::DeviceInfo> video_info(webrtc::VideoCaptureFactory::CreateDeviceInfo());

    PyObject *retval = NULL;
    if (video_info) {
        int num_devices = video_info->NumberOfDevices();
        retval = PyList_New(num_devices);

        for (int i = 0; i < num_devices; ++i) {
            const uint32_t kSize = 256;
            char name[kSize] = { 0 };
            char id[kSize] = { 0 };

            PyObject *device = PyDict_New();

            if (video_info->GetDeviceName(i, name, kSize, id, kSize) != -1) {
                PyDict_SetItemString(device, "kind", PyUnicode_FromString("video"));
                PyDict_SetItemString(device, "label", PyUnicode_FromString(name));
                PyDict_SetItemString(device, "id", PyUnicode_FromString(id));
                PyList_SetItem(retval, index, device);
                index++;
            }
        }
    }

    return retval;
}
