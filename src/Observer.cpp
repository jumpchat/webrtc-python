#include "Observer.h"
#include "MediaStream.h"
#include "SessionDescription.h"
#include "Utils.h"

#include <stdarg.h>

using namespace WebRTC;

/// SDPObserver - CreateOffer/CreateAnswer

SDPObserver::SDPObserver(PyObject* future)
    : _future(future)
{
    LOG(LS_INFO) << __PRETTY_FUNCTION__;
    if (_future) {
        Py_IncRef(_future);
    }
}

SDPObserver::~SDPObserver()
{
    LOG(LS_INFO) << __PRETTY_FUNCTION__;
    if (_future) {
        Py_DecRef(_future);
    }
}

void SDPObserver::OnSuccess(webrtc::SessionDescriptionInterface* desc)
{
    LOG(LS_INFO) << __PRETTY_FUNCTION__;
    PythonGILLock lock;
    std::string sdpString;
    desc->ToString(&sdpString);
    PyObject* sdp = PyDict_New();
    PyDict_SetItemString(sdp, "type", PyUnicode_FromString(desc->type().c_str()));
    PyDict_SetItemString(sdp, "sdp", PyUnicode_FromString(sdpString.c_str()));
    PyObject_CallMethodObjArgs(_future, PyUnicode_FromString("set_result"), sdp, NULL);
}

void SDPObserver::OnFailure(const std::string& error)
{
    LOG(LS_INFO) << __PRETTY_FUNCTION__;
    PythonGILLock lock;
    PyErr_SetString(PyExc_Exception, error.c_str());
}

/// SetSDPObserver - SetLocalSessionDescription/SetRemoteSessionDescription

SetSDPObserver::SetSDPObserver(PyObject* future)
    : _future(future)
{
    LOG(LS_INFO) << __PRETTY_FUNCTION__;
    if (_future) {
        Py_IncRef(_future);
    }
}

SetSDPObserver::~SetSDPObserver()
{
    LOG(LS_INFO) << __PRETTY_FUNCTION__;
    if (_future) {
        Py_DecRef(_future);
    }
}

void SetSDPObserver::OnSuccess()
{
    LOG(LS_INFO) << __PRETTY_FUNCTION__;
    PythonGILLock lock;
    PyObject_CallMethodObjArgs(_future, PyUnicode_FromString("set_result"), Py_True, NULL);
}

void SetSDPObserver::OnFailure(const std::string& error)
{
    LOG(LS_INFO) << __PRETTY_FUNCTION__;
    PythonGILLock lock;
    PyErr_SetString(PyExc_Exception, error.c_str());
}

/// PCObserver - PeerConnection events

struct IceCandidate {
    std::string sdp_mid;
    int sdp_mline_index;
    std::string candidate;

    IceCandidate() {}
    ~IceCandidate() {}
};

struct PCOMethodData {
    PCObserver* pco;
    IceCandidate ice_candidate;
    union {
        webrtc::PeerConnectionInterface::SignalingState signaling_state;
        webrtc::PeerConnectionInterface::IceConnectionState ice_connection_state;
        webrtc::MediaStreamInterface* stream;
        webrtc::PeerConnectionInterface::IceGatheringState ice_gathering_state;
        webrtc::DataChannelInterface* data_channel;
    } data;

    PCOMethodData() {}
    ~PCOMethodData() {}
};

PCObserver::PCObserver(PeerConnection* pc)
    : _pc(pc)
    , _event_handler(NULL)
    , _observer(NULL)
{
    LOG(LS_INFO) << __PRETTY_FUNCTION__;
}

PCObserver::~PCObserver()
{
    LOG(LS_INFO) << __PRETTY_FUNCTION__;
    SetObserver(NULL, NULL);
}

void PCObserver::SetObserver(PyObject* observer, PyObject* event_handler)
{
    LOG(LS_INFO) << __PRETTY_FUNCTION__;
    if (_observer) {
        Py_DecRef(_observer);
    }
    _observer = observer;
    if (_observer) {
        Py_IncRef(_observer);
    }

    if (_event_handler) {
        Py_DecRef(_event_handler);
    }
    _event_handler = event_handler;
    if (_event_handler) {
        Py_IncRef(_event_handler);
    }
}

void PCObserver::SetPeerConnection(PeerConnection* pc)
{
    LOG(LS_INFO) << __PRETTY_FUNCTION__;
    _pc = pc;
}

void PCObserver::OnSignalingChange(webrtc::PeerConnectionInterface::SignalingState new_state)
{
    LOG(LS_INFO) << __PRETTY_FUNCTION__;

    PCOMethodData* data = new PCOMethodData();
    data->pco = this;
    data->data.signaling_state = new_state;
    Py_AddPendingCall(PCObserver::OnSignalingChangeCb, data);
}

int PCObserver::OnSignalingChangeCb(void* data)
{
    PCOMethodData* pcomd = reinterpret_cast<PCOMethodData*>(data);

    PythonGILLock lock;
    PyObject* _observer = pcomd->pco->_observer;
    webrtc::PeerConnectionInterface::SignalingState new_state = pcomd->data.signaling_state;
    PyObject* state;
    switch (new_state) {
    case webrtc::PeerConnectionInterface::kStable:
        state = PyUnicode_FromString("stable");
        break;
    case webrtc::PeerConnectionInterface::kHaveLocalOffer:
        state = PyUnicode_FromString("have-local-offer");
        break;
    case webrtc::PeerConnectionInterface::kHaveLocalPrAnswer:
        state = PyUnicode_FromString("have-local-pranswer");
        break;
    case webrtc::PeerConnectionInterface::kHaveRemoteOffer:
        state = PyUnicode_FromString("have-remote-offer");
        break;
    case webrtc::PeerConnectionInterface::kHaveRemotePrAnswer:
        state = PyUnicode_FromString("have-remove-pranswer");
        break;
    case webrtc::PeerConnectionInterface::kClosed:
        state = PyUnicode_FromString("closed");
        break;
    default:
        state = PyUnicode_FromString("unknown");
        break;
    }

    // call python method
    pcomd->pco->CallMethod(_observer, "onsignalingstatechange", state, NULL);
    // Py_DecRef(state);
    delete pcomd;
    return 0;
}

void PCObserver::OnAddStream(rtc::scoped_refptr<webrtc::MediaStreamInterface> stream)
{
    LOG(LS_INFO) << __PRETTY_FUNCTION__;

    PCOMethodData* data = new PCOMethodData();
    data->pco = this;
    data->data.stream = stream.get();
    Py_AddPendingCall(PCObserver::OnAddStreamCb, data);
}

void PCObserver::OnAddStream(webrtc::MediaStreamInterface* stream)
{
    LOG(LS_INFO) << __PRETTY_FUNCTION__;

    // PCOMethodData* data = new PCOMethodData();
    // data->pco = this;
    // data->data.stream = stream;
    // Py_AddPendingCall(PCObserver::OnAddStreamCb, data);
}

int PCObserver::OnAddStreamCb(void* data)
{
    PCOMethodData* pcomd = reinterpret_cast<PCOMethodData*>(data);

    PythonGILLock lock;
    PyObject* _observer = pcomd->pco->_observer;
    webrtc::MediaStreamInterface* stream = pcomd->data.stream;

    PyObject* media_stream = MediaStream::New(stream);
    pcomd->pco->CallMethod(_observer, "onaddstream", media_stream, NULL);

    // Py_DecRef(media_stream);
    delete pcomd;
    return 0;
}

void PCObserver::OnRemoveStream(rtc::scoped_refptr<webrtc::MediaStreamInterface> stream)
{
    LOG(LS_INFO) << __PRETTY_FUNCTION__;

    PCOMethodData* data = new PCOMethodData();
    data->pco = this;
    data->data.stream = stream.get();
    Py_AddPendingCall(PCObserver::OnRemoveStreamCb, data);
}

void PCObserver::OnRemoveStream(webrtc::MediaStreamInterface* stream)
{
    LOG(LS_INFO) << __PRETTY_FUNCTION__;

    // PCOMethodData* data = new PCOMethodData();
    // data->pco = this;
    // data->data.stream = stream;
    // Py_AddPendingCall(PCObserver::OnRemoveStreamCb, data);
}

int PCObserver::OnRemoveStreamCb(void* data)
{
    PCOMethodData* pcomd = reinterpret_cast<PCOMethodData*>(data);

    PythonGILLock lock;
    PyObject* _observer = pcomd->pco->_observer;
    webrtc::MediaStreamInterface* stream = pcomd->data.stream;

    PyObject* media_stream = MediaStream::New(stream);
    pcomd->pco->CallMethod(_observer, "onremovestream", media_stream, NULL);

    // Py_DecRef(media_stream);
    delete pcomd;
    return 0;
}

void PCObserver::OnDataChannel(rtc::scoped_refptr<webrtc::DataChannelInterface> data_channel)
{
    LOG(LS_INFO) << __PRETTY_FUNCTION__;
    OnDataChannel(data_channel.get());
}

void PCObserver::OnDataChannel(webrtc::DataChannelInterface* data_channel)
{
    LOG(LS_INFO) << __PRETTY_FUNCTION__;
    //TODO:
}

void PCObserver::OnRenegotiationNeeded()
{
    LOG(LS_INFO) << __PRETTY_FUNCTION__;
    PCOMethodData* data = new PCOMethodData();
    data->pco = this;
    Py_AddPendingCall(PCObserver::OnRenegotiationNeededCb, data);
}

int PCObserver::OnRenegotiationNeededCb(void* data)
{
    LOG(LS_INFO) << __PRETTY_FUNCTION__;

    PCOMethodData* pcomd = reinterpret_cast<PCOMethodData*>(data);

    PythonGILLock lock;
    PyObject* _observer = pcomd->pco->_observer;
    pcomd->pco->CallMethod(_observer, "onnegotiationneeded", NULL);

    return 0;
}

void PCObserver::OnIceConnectionChange(webrtc::PeerConnectionInterface::IceConnectionState new_state)
{
    LOG(LS_INFO) << __PRETTY_FUNCTION__;

    PCOMethodData* data = new PCOMethodData();
    data->pco = this;
    data->data.ice_connection_state = new_state;
    Py_AddPendingCall(PCObserver::OnIceConnectionChangeCb, data);
}

int PCObserver::OnIceConnectionChangeCb(void* data)
{
    LOG(LS_INFO) << __PRETTY_FUNCTION__;

    PCOMethodData* pcomd = reinterpret_cast<PCOMethodData*>(data);

    PythonGILLock lock;
    PyObject* _observer = pcomd->pco->_observer;
    webrtc::PeerConnectionInterface::IceConnectionState new_state = pcomd->data.ice_connection_state;
    PyObject* state;
    switch (new_state) {
    case webrtc::PeerConnectionInterface::kIceConnectionNew:
        state = PyUnicode_FromString("new");
        break;
    case webrtc::PeerConnectionInterface::kIceConnectionChecking:
        state = PyUnicode_FromString("checking");
        break;
    case webrtc::PeerConnectionInterface::kIceConnectionConnected:
        state = PyUnicode_FromString("connected");
        break;
    case webrtc::PeerConnectionInterface::kIceConnectionCompleted:
        state = PyUnicode_FromString("completed");
        break;
    case webrtc::PeerConnectionInterface::kIceConnectionFailed:
        state = PyUnicode_FromString("failed");
        break;
    case webrtc::PeerConnectionInterface::kIceConnectionDisconnected:
        state = PyUnicode_FromString("disconnected");
        break;
    case webrtc::PeerConnectionInterface::kIceConnectionClosed:
        state = PyUnicode_FromString("closed");
        break;
    default:
        state = PyUnicode_FromString("unknown");
        break;
    }
    pcomd->pco->CallMethod(_observer, "oniceconnectionstatechange", state, NULL);
    // Py_DecRef(state);
    delete pcomd;
    return 0;
}

void PCObserver::OnIceGatheringChange(webrtc::PeerConnectionInterface::IceGatheringState new_state)
{
    LOG(LS_INFO) << __PRETTY_FUNCTION__;

    PCOMethodData* data = new PCOMethodData();
    data->pco = this;
    data->data.ice_gathering_state = new_state;
    Py_AddPendingCall(PCObserver::OnIceGatheringChangeCb, data);
}

int PCObserver::OnIceGatheringChangeCb(void *data) {
    LOG(LS_INFO) << __PRETTY_FUNCTION__;

    PCOMethodData* pcomd = reinterpret_cast<PCOMethodData*>(data);

    PythonGILLock lock;
    PyObject* _observer = pcomd->pco->_observer;
    webrtc::PeerConnectionInterface::IceGatheringState new_state = pcomd->data.ice_gathering_state;
    PyObject* state;
    switch (new_state) {
    case webrtc::PeerConnectionInterface::kIceGatheringNew:
        state = PyUnicode_FromString("new");
        break;
    case webrtc::PeerConnectionInterface::kIceGatheringGathering:
        state = PyUnicode_FromString("gathering");
        break;
    case webrtc::PeerConnectionInterface::kIceGatheringComplete:
        state = PyUnicode_FromString("completed");
        break;
    default:
        state = PyUnicode_FromString("unknown");
        break;
    }
    pcomd->pco->CallMethod(_observer, "onicegatheringstatechange", state, NULL);
    // Py_DecRef(state);
    delete pcomd;
    return 0;
}

void PCObserver::OnIceCandidate(const webrtc::IceCandidateInterface* candidate)
{
    LOG(LS_INFO) << __PRETTY_FUNCTION__;

    PCOMethodData* data = new PCOMethodData();
    data->pco = this;
    if (candidate) {
        data->ice_candidate.sdp_mid = candidate->sdp_mid();
        data->ice_candidate.sdp_mline_index = candidate->sdp_mline_index();
        candidate->ToString(&data->ice_candidate.candidate);
    }
    Py_AddPendingCall(PCObserver::OnIceCandidateCb, data);
}

int PCObserver::OnIceCandidateCb(void* data)
{
    LOG(LS_INFO) << __PRETTY_FUNCTION__;

    PCOMethodData* pcomd = reinterpret_cast<PCOMethodData*>(data);

    PythonGILLock lock;
    PyObject* _observer = pcomd->pco->_observer;
    IceCandidate &candidate = pcomd->ice_candidate;

    if (candidate.sdp_mid.empty()) {
        pcomd->pco->CallMethod(_observer, "onicecandidate", Py_None, NULL);
    } else {
        PyObject* iceCandidate = PyDict_New();
        PyDict_SetItemString(iceCandidate, "sdpMid", PyUnicode_FromString(candidate.sdp_mid.c_str()));
        PyDict_SetItemString(iceCandidate, "sdpMLineIndex", PyLong_FromLong(candidate.sdp_mline_index));
        PyDict_SetItemString(iceCandidate, "candidate", PyUnicode_FromString(candidate.candidate.c_str()));

        pcomd->pco->CallMethod(_observer, "onicecandidate", iceCandidate, NULL);
        // Py_DecRef(iceCandidate);
    }

    delete pcomd;
    return 0;
}

void PCObserver::OnIceCandidatesRemoved(const std::vector<cricket::Candidate>& candidates)
{
    LOG(LS_INFO) << __PRETTY_FUNCTION__;
}

void PCObserver::OnIceConnectionReceivingChange(bool receiving)
{
    LOG(LS_INFO) << __PRETTY_FUNCTION__;
}

void PCObserver::OnAddTrack(rtc::scoped_refptr<webrtc::RtpReceiverInterface> receiver,
    const std::vector<rtc::scoped_refptr<webrtc::MediaStreamInterface>>& streams)
{
    LOG(LS_INFO) << __PRETTY_FUNCTION__;
}

// last parameter must be NULL
void PCObserver::CallMethod(PyObject* obj, const char* method_name, ...)
{
    LOG(LS_INFO) << __PRETTY_FUNCTION__;

    va_list vl;
    va_start(vl, method_name);

    std::vector<PyObject*> args_list;
    PyObject* pyo;
    while (NULL != (pyo = va_arg(vl, PyObject*))) {
        args_list.push_back(pyo);
    }

    PyObject* args = PyTuple_New(args_list.size());
    for (size_t i = 0; i < args_list.size(); i++) {
        PyTuple_SetItem(args, i, args_list[i]);
    }

    PyObject* name = PyUnicode_FromString(method_name);
    PyObject* item = PyTuple_Pack(3, obj, name, args);

    // call
    PyObject* put = PyUnicode_FromString("put");
    PyObject* method = PyObject_GetAttr(_event_handler, put);
    if (method != NULL && method != Py_None && _event_handler != NULL) {
        PyObject_CallMethodObjArgs(_event_handler, put, item, NULL);
    }
    if (PyErr_ExceptionMatches(PyExc_AttributeError)) {
        PyErr_Clear();
    }
    Py_DecRef(put);
    Py_DecRef(method);

    Py_DecRef(args);
    Py_DecRef(name);
    Py_DecRef(item);

    va_end(vl);
}
