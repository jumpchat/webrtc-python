#ifndef WEBRTC_GETUSERMEDIA_H
#define WEBRTC_GETUSERMEDIA_H

#include "Common.h"

namespace WebRTC {

class GetUserMedia {
    public:
        static void GetMediaStream(PyObject *constraints, PyObject *successCallback, PyObject *errorCallback);
};

}

#endif // WEBRTC_GETUSERMEDIA_H