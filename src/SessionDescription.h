#ifndef __WEBRTC_SESSION_DESCRIPTION_H__
#define __WEBRTC_SESSION_DESCRIPTION_H__

#include "Common.h"

namespace WebRTC {
class SessionDescription {
    public:
        static PyObject* New(webrtc::SessionDescriptionInterface* sdp);
        SessionDescription(const std::string &type, const std::string &sdp);
        SessionDescription(webrtc::SessionDescriptionInterface *sdp);
        ~SessionDescription();

        std::string type();
        std::string sdp();

    private:
        webrtc::SessionDescriptionInterface *_sdp;
};
}


#endif // __WEBRTC_SESSION_DESCRIPTION_H__
