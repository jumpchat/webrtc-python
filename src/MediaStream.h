#ifndef WEBRTC_MEDIASTREAM_H
#define WEBRTC_MEDIASTREAM_H

#include "Common.h"
#include "MediaStreamTrack.h"

namespace WebRTC {
class MediaStream {
friend class PeerConnection;
public:
    static PyObject* New(rtc::scoped_refptr<webrtc::MediaStreamInterface> stream);

    MediaStream();
    ~MediaStream();

    std::string label();

    std::vector<MediaStreamTrack*> GetAudioTracks();
    std::vector<MediaStreamTrack*> GetVideoTracks();
    void AddTrack(MediaStreamTrack* track);
    void RemoveTrack(MediaStreamTrack* track);

private:
    rtc::scoped_refptr<webrtc::MediaStreamInterface> _stream;
};
}

#endif // WEBRTC_MEDIASTREAM_H
