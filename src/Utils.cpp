#include "Utils.h"

using namespace WebRTC;

PythonGILLock::PythonGILLock() {
    LOG(LS_INFO) << __PRETTY_FUNCTION__;
    _state = PyGILState_Ensure();
}

PythonGILLock::~PythonGILLock() {
    LOG(LS_INFO) << __PRETTY_FUNCTION__;
    PyGILState_Release(_state);
}