#include "Common.h"
#include "MediaStream.h"

using namespace WebRTC;

MediaStream::MediaStream() {
    LOG(LS_INFO) << __PRETTY_FUNCTION__;
}

MediaStream::~MediaStream() {
    LOG(LS_INFO) << __PRETTY_FUNCTION__;
}

std::string MediaStream::label() {
    LOG(LS_INFO) << __PRETTY_FUNCTION__;
    if (_stream.get())
        return _stream->label();
    return "";
}


std::vector<MediaStreamTrack*> MediaStream::GetAudioTracks() {
    LOG(LS_INFO) << __PRETTY_FUNCTION__;
    std::vector<rtc::scoped_refptr<webrtc::AudioTrackInterface> > tracks = _stream->GetAudioTracks();
    std::vector<MediaStreamTrack *> retval;
    for (size_t i = 0; i < tracks.size(); i++) {
        retval.push_back(MediaStreamTrack::New(tracks[i].get()));
    }
    return retval;
}

std::vector<MediaStreamTrack*> MediaStream::GetVideoTracks() {
    LOG(LS_INFO) << __PRETTY_FUNCTION__;
    std::vector<rtc::scoped_refptr<webrtc::VideoTrackInterface> > tracks = _stream->GetVideoTracks();
    std::vector<MediaStreamTrack *> retval;
    for (size_t i = 0; i < tracks.size(); i++) {
        retval.push_back(MediaStreamTrack::New(tracks[i].get()));
    }
    return retval;
}

void MediaStream::AddTrack(MediaStreamTrack* track) {
    LOG(LS_INFO) << __PRETTY_FUNCTION__;
    if (track->_audio)
        _stream->AddTrack(static_cast<webrtc::AudioTrackInterface*>(track->_track.get()));
    else
        _stream->AddTrack(static_cast<webrtc::VideoTrackInterface*>(track->_track.get()));
}

void MediaStream::RemoveTrack(MediaStreamTrack* track) {
    LOG(LS_INFO) << __PRETTY_FUNCTION__;
    if (track->_audio)
        _stream->RemoveTrack(static_cast<webrtc::AudioTrackInterface*>(track->_track.get()));
    else
        _stream->RemoveTrack(static_cast<webrtc::VideoTrackInterface*>(track->_track.get()));
}
