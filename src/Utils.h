#ifndef __WEBRTC_UTILS_H__
#define __WEBRTC_UTILS_H__

#include "Common.h"

namespace WebRTC {
class PythonGILLock {
public:
    PythonGILLock();
    ~PythonGILLock();

private:
    PyGILState_STATE _state;
};
}

#endif // __WEBRTC_UTILS_H__