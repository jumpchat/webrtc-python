#ifndef __WEBRTC_OBSERVER_H__
#define __WEBRTC_OBSERVER_H__

#include "Common.h"

namespace WebRTC {

class PeerConnection;

class SDPObserver : public rtc::RefCountedObject<webrtc::CreateSessionDescriptionObserver> {
public:
    SDPObserver(PyObject* future);
    virtual ~SDPObserver();

    virtual void OnSuccess(webrtc::SessionDescriptionInterface* desc);
    virtual void OnFailure(const std::string& error);

private:
    PyObject* _future;
};

class SetSDPObserver : public rtc::RefCountedObject<webrtc::SetSessionDescriptionObserver> {
public:
    SetSDPObserver(PyObject* future);
    virtual ~SetSDPObserver();

    virtual void OnSuccess();
    virtual void OnFailure(const std::string& error);

private:
    PyObject* _future;
};

class PCObserver : public rtc::RefCountedObject<webrtc::PeerConnectionObserver> {
    friend class PeerConnection;

public:
    PCObserver(PeerConnection* pc);
    ~PCObserver();

    void SetObserver(PyObject *observer, PyObject *event_handler);
    void SetPeerConnection(PeerConnection *pc);

    // webrtc::PeerConnectionObserver overrides
    virtual void OnSignalingChange(webrtc::PeerConnectionInterface::SignalingState new_state);
    virtual void OnAddStream(rtc::scoped_refptr<webrtc::MediaStreamInterface> stream);
    virtual void OnAddStream(webrtc::MediaStreamInterface* stream);
    virtual void OnRemoveStream(rtc::scoped_refptr<webrtc::MediaStreamInterface> stream);
    virtual void OnRemoveStream(webrtc::MediaStreamInterface* stream);
    virtual void OnDataChannel(rtc::scoped_refptr<webrtc::DataChannelInterface> data_channel);
    virtual void OnDataChannel(webrtc::DataChannelInterface* data_channel);
    virtual void OnRenegotiationNeeded();
    virtual void OnIceConnectionChange(webrtc::PeerConnectionInterface::IceConnectionState new_state);
    virtual void OnIceGatheringChange(webrtc::PeerConnectionInterface::IceGatheringState new_state);
    virtual void OnIceCandidate(const webrtc::IceCandidateInterface* candidate);
    virtual void OnIceCandidatesRemoved(const std::vector<cricket::Candidate>& candidates);
    virtual void OnIceConnectionReceivingChange(bool receiving);
    virtual void OnAddTrack(rtc::scoped_refptr<webrtc::RtpReceiverInterface> receiver,
        const std::vector<rtc::scoped_refptr<webrtc::MediaStreamInterface>>& streams);

    static int OnIceCandidateCb(void *data);
    static int OnAddStreamCb(void *data);
    static int OnRemoveStreamCb(void *data);
    static int OnSignalingChangeCb(void *data);
    static int OnIceConnectionChangeCb(void *data);
    static int OnIceGatheringChangeCb(void *data);
    static int OnRenegotiationNeededCb(void *data);

protected:
    void CallMethod(PyObject *obj, const char *method_name, ...);

private:
    PeerConnection* _pc;
    PyObject *_event_handler;
    PyObject *_observer;
};
}

#endif // __WEBRTC_OBSERVER_H__
