#ifndef WEBRTC_PEERCONNECTION_H
#define WEBRTC_PEERCONNECTION_H

#include "Common.h"

namespace WebRTC {
class PCObserver;
class MediaStream;
class MediaConstraints;

class PeerConnection {
public:
    PeerConnection(PyObject* config, PyObject* constraints);
    virtual ~PeerConnection();

    bool AddIceCandidate(PyObject* candidate);
    void AddStream(WebRTC::MediaStream* stream);
    void RemoveStream(WebRTC::MediaStream* stream);
    void CreateOffer(PyObject* constraints, PyObject* future);
    void CreateAnswer(PyObject* constraints, PyObject* future);
    void SetLocalSessionDescription(PyObject *sdp, PyObject* future);
    void SetRemoteSessionDescription(PyObject *sdp, PyObject* future);
    void Close();

    void SetObserver(PyObject *observer, PyObject *event_handler);

private:
    rtc::scoped_refptr<webrtc::PeerConnectionInterface> _pc;
    rtc::scoped_refptr<PCObserver> _observer;
};
}

#endif // WEBRTC_PEERCONNECTION_H