#include "GetUserMedia.h"
#include "Common.h"
#include "MediaConstraints.h"
#include "Platform.h"
#include "GetSources.h"
#include "MediaStream.h"

using namespace WebRTC;

void GetUserMedia::GetMediaStream(PyObject* constraints_value, PyObject* successCallback, PyObject* errorCallback)
{
    LOG(LS_INFO) << __PRETTY_FUNCTION__;

    rtc::scoped_refptr<webrtc::MediaStreamInterface> stream;
    rtc::scoped_refptr<MediaConstraints> constraints = MediaConstraints::New(constraints_value);
    const char* error = 0;
    bool have_source = false;

    std::string audioId = constraints->AudioId();
    std::string videoId = constraints->VideoId();

    if (constraints->UseAudio() || constraints->UseVideo()) {
        rtc::scoped_refptr<webrtc::PeerConnectionFactoryInterface> factory = Platform::GetFactory();

        if (factory.get()) {
            stream = factory->CreateLocalMediaStream("stream");

            if (stream.get()) {
                if (constraints->UseAudio()) {
                    rtc::scoped_refptr<webrtc::AudioTrackInterface> audio_track;

                    if (audioId.empty()) {
                        audio_track = GetSources::GetAudioSource(constraints);
                    } else {
                        audio_track = GetSources::GetAudioSource(audioId, constraints);
                    }

                    if (audio_track.get()) {
                        if (!stream->AddTrack(audio_track)) {
                            error = "Invalid Audio Input";
                        } else {
                            have_source = true;
                        }
                    } else {
                        if (!audioId.empty()) {
                            error = "Invalid Audio Input";
                        }
                    }
                }
            }

            if (constraints->UseVideo()) {
                rtc::scoped_refptr<webrtc::VideoTrackInterface> video_track;

                if (videoId.empty()) {
                    video_track = GetSources::GetVideoSource(constraints);
                } else {
                    video_track = GetSources::GetVideoSource(videoId, constraints);
                }

                if (video_track.get()) {
                    if (!stream->AddTrack(video_track)) {
                        error = "Invalid Video Input";
                    } else {
                        have_source = true;
                    }
                } else {
                    if (!videoId.empty()) {
                        error = "Invalid Video Input";
                    }
                }
            }
        } else {
            error = "Internal Error";
        }
    }

    if (!have_source) {
        error = "No available inputs";
    }

    PyObject *mediaStream = NULL;
    if (!error) {
        if (stream.get()) {
            mediaStream = MediaStream::New(stream);
            if (!mediaStream) {
                error = "Cannot create media stream wrapper";
            }
        } else {
            error = "Invalid MediaStream";
        }
    }

    if (error) {
        // printf("Calling errorCallback: %s\n", error);
        // PyObject *argsList = PyList_New(1);
        // PyList_SetItem(argsList, 0, PyUnicode_FromString(error));
        PyObject_CallFunctionObjArgs(errorCallback, PyUnicode_FromString(error), NULL);
        // Py_DecRef(argsList);
    } else {
        // printf("Calling successCallback: %s\n", stream->label().c_str());
        // PyObject *argsList = PyTuple_New(1);
        // PyTuple_SetItem(argsList, 0, mediaStream);
        PyObject_CallFunctionObjArgs(successCallback, mediaStream, NULL);
        // Py_DecRef(argsList);
    }
}