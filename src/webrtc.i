%module webrtc
%include "std_string.i"
%include "std_vector.i"

%{
#include "Common.h"
#include "Platform.h"
#include "PeerConnection.h"
#include "GetUserMedia.h"
#include "GetSources.h"
#include "MediaConstraints.h"
#include "MediaStream.h"
#include "MediaStreamTrack.h"
#include "SessionDescription.h"
#include "VideoRenderer.h"
%}

namespace std {
   %template(MediaStreamTrackVector) vector<WebRTC::MediaStreamTrack*>;
}

namespace WebRTC {
    class Platform {
        public:
            static void Init();
            static void Dispose();
            static void SetDebug(const std::string &);
    };

    class PeerConnection {
        public:
            PeerConnection(PyObject *config, PyObject *constraints);
            ~PeerConnection();

            bool AddIceCandidate(PyObject *candidate);
            void AddStream(WebRTC::MediaStream *stream);
            void RemoveStream(WebRTC::MediaStream *stream);
            void CreateOffer(PyObject* constraints, PyObject* future);
            void CreateAnswer(PyObject* constraints, PyObject* future);
            void SetLocalSessionDescription(PyObject *sdp, PyObject* future);
            void SetRemoteSessionDescription(PyObject *sdp, PyObject* future);
            void Close();

            void SetObserver(PyObject *observer, PyObject *event_handler);
    };

    class VideoRenderer {
        public:
            VideoRenderer();
            ~VideoRenderer();

            void SetOnFrame(PyObject *callback);
            PyObject *GetOnFrame();
            void SetEventHandler(PyObject *event_handler);
    };

    class MediaStreamTrack {
        public:
            MediaStreamTrack();
            ~MediaStreamTrack();

            bool GetEnabled();
            void SetEnabled(bool enabled);
            std::string Id();
            std::string Kind();
            std::string State();
            bool IsAudioTrack();
            bool IsVideoTrack();

            void AddRenderer(VideoRenderer *);
            void RemoveRenderer(VideoRenderer *);
    };

    class MediaStream {
        public:
            MediaStream();
            ~MediaStream();

            std::string label();

            std::vector<MediaStreamTrack*> GetAudioTracks();
            std::vector<MediaStreamTrack*> GetVideoTracks();
            void AddTrack(MediaStreamTrack* track);
            void RemoveTrack(MediaStreamTrack* track);

    };

    class GetUserMedia {
        public:
            static void GetMediaStream(PyObject *constraints,
                PyObject *successCallback,
                PyObject *errorCallback);
    };

    class GetSources {
        public:
            static PyObject* GetDevices();
    };

    class SessionDescription {
        public:
            SessionDescription(const std::string &type, const std::string &sdp);
            ~SessionDescription();
    };

}

%inline %{

namespace WebRTC {
PyObject *MediaStream::New(rtc::scoped_refptr<webrtc::MediaStreamInterface> stream) {
    MediaStream *mediaStream = new MediaStream();
    mediaStream->_stream = stream;

    PyObject *retval = SWIG_NewPointerObj((void*)(mediaStream), SWIGTYPE_p_WebRTC__MediaStream, SWIG_POINTER_OWN);
    return retval;
}

PyObject* SessionDescription::New(webrtc::SessionDescriptionInterface* sdp) {
    SessionDescription *sdpObj = new SessionDescription(sdp);

    PyObject *retval = SWIG_NewPointerObj((void*)(sdpObj), SWIGTYPE_p_WebRTC__SessionDescription, SWIG_POINTER_OWN);
    return retval;
}
}
%}