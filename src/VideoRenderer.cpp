#include "VideoRenderer.h"
#include "Utils.h"

using namespace WebRTC;

VideoRenderer::VideoRenderer()
    : _callback(NULL)
    , _event_handler(NULL)
{
    LOG(LS_INFO) << __PRETTY_FUNCTION__;
}

VideoRenderer::~VideoRenderer()
{
    LOG(LS_INFO) << __PRETTY_FUNCTION__;
    SetOnFrame(NULL);

    if (_track.get()) {
        _track->RemoveSink(this);
    }
}

void VideoRenderer::SetOnFrame(PyObject* callback)
{
    LOG(LS_INFO) << __PRETTY_FUNCTION__;
    if (_callback) {
        Py_DecRef(_callback);
    }
    _callback = callback;
    if (_callback) {
        Py_IncRef(_callback);
    }
}

PyObject* VideoRenderer::GetOnFrame()
{
    LOG(LS_INFO) << __PRETTY_FUNCTION__;
    return _callback;
}

// callback from webrtc
void VideoRenderer::OnFrame(const webrtc::VideoFrame& frame)
{
    LOG(LS_INFO) << __PRETTY_FUNCTION__;
    if (_callback) {
        // PythonGILLock lock;
        // PyGILState_STATE state = PyGILState_Ensure();

        // PyObject_Print(_callback, stdout, 0);

        //_frame = frame.video_frame_buffer();
        _frame = webrtc::I420Buffer::Copy(*frame.video_frame_buffer().get());
        // PythonGILLock lock;
        // VideoRenderer::OnFrameCb(this);
        Py_AddPendingCall(VideoRenderer::OnFrameCb, this);
        // PyGILState_Release(state);
    }
}

int VideoRenderer::OnFrameCb(void* arg)
{
    LOG(LS_INFO) << __PRETTY_FUNCTION__;
    VideoRenderer *self = reinterpret_cast<VideoRenderer *>(arg);
    PyObject* frameObject = PyDict_New();
    rtc::scoped_refptr<webrtc::I420Buffer> _frame = self->_frame;
    PyDict_SetItemString(frameObject, "width", PyLong_FromLong(_frame->width()));
    PyDict_SetItemString(frameObject, "height", PyLong_FromLong(_frame->height()));
    const char* y = reinterpret_cast<const char*>(_frame->MutableDataY());
    const char* u = reinterpret_cast<const char*>(_frame->MutableDataU());
    const char* v = reinterpret_cast<const char*>(_frame->MutableDataV());
    PyDict_SetItemString(frameObject, "y",
        PyByteArray_FromStringAndSize(y, _frame->StrideY() * _frame->height()));
    PyDict_SetItemString(frameObject, "u",
        PyByteArray_FromStringAndSize(u, _frame->StrideU() * _frame->height() / 2));
    PyDict_SetItemString(frameObject, "v",
        PyByteArray_FromStringAndSize(v, _frame->StrideV() * _frame->height() / 2));

    // PyObject_CallFunctionObjArgs(self->_callback, frameObject, NULL);
    self->CallMethod(self->_callback, "onframe", frameObject, NULL);
    // Py_DecRef(frameObject);
    return 0;
}

void VideoRenderer::SetTrack(webrtc::VideoTrackInterface *track) {
    _track = track;
}

void VideoRenderer::CallMethod(PyObject* obj, const char* method_name, ...)
{
    LOG(LS_INFO) << __PRETTY_FUNCTION__;

    va_list vl;
    va_start(vl, method_name);

    std::vector<PyObject*> args_list;
    PyObject* pyo;
    while (NULL != (pyo = va_arg(vl, PyObject*))) {
        args_list.push_back(pyo);
    }

    PyObject* args = PyTuple_New(args_list.size());
    for (size_t i = 0; i < args_list.size(); i++) {
        PyTuple_SetItem(args, i, args_list[i]);
    }

    PyObject* name = PyUnicode_FromString(method_name);
    PyObject* item = PyTuple_Pack(3, obj, name, args);

    // call
    PyObject* put = PyUnicode_FromString("put");
    PyObject* method = PyObject_GetAttr(_event_handler, put);
    if (method != NULL && method != Py_None && _event_handler != NULL) {
        PyObject_CallMethodObjArgs(_event_handler, put, item, NULL);
    }
    if (PyErr_ExceptionMatches(PyExc_AttributeError)) {
        PyErr_Clear();
    }
    Py_DecRef(put);
    Py_DecRef(method);

    Py_DecRef(args);
    Py_DecRef(name);
    Py_DecRef(item);

    va_end(vl);
}