#include "Platform.h"
#include "Common.h"
#include "PeerConnection.h"

using namespace WebRTC;

uint32_t counter = 0;

rtc::scoped_refptr<webrtc::PeerConnectionFactoryInterface> Platform::_factory;
rtc::Thread Platform::_signal_thread;
rtc::Thread Platform::_worker_thread;
rtc::Thread Platform::_network_thread;

void Platform::Init()
{
    LOG(LS_INFO) << __PRETTY_FUNCTION__;

    PyEval_InitThreads();

#if defined(WEBRTC_WIN)
    rtc::EnsureWinsockInit();
#endif

    rtc::InitializeSSL();

    _signal_thread.Start();
    _worker_thread.Start();
    _network_thread.Start();

    // initialize classes
    _factory = webrtc::CreatePeerConnectionFactory(&_network_thread, &_worker_thread, &_signal_thread, nullptr, 0, 0);
}

void Platform::Dispose()
{
    LOG(LS_INFO) << __PRETTY_FUNCTION__;

    _factory = NULL;

    _signal_thread.SetAllowBlockingCalls(true);
    _signal_thread.Stop();
    _worker_thread.SetAllowBlockingCalls(true);
    _worker_thread.Stop();
    _network_thread.SetAllowBlockingCalls(true);
    _network_thread.Stop();

    rtc::CleanupSSL();
}

void Platform::SetDebug(const std::string &id) {
    if (id == "verbose") {
        rtc::LogMessage::LogToDebug(rtc::LS_VERBOSE);
    } else if (id == "info") {
        rtc::LogMessage::LogToDebug(rtc::LS_INFO);
    } else if (id == "warning") {
        rtc::LogMessage::LogToDebug(rtc::LS_WARNING);
    } else if (id == "error") {
        rtc::LogMessage::LogToDebug(rtc::LS_ERROR);
    } else {
        rtc::LogMessage::LogToDebug(rtc::LS_NONE);
    }
}

rtc::scoped_refptr<webrtc::PeerConnectionFactoryInterface> Platform::GetFactory()
{
    if (!_factory.get()) {
        Platform::Init();
    }
    return _factory;
}


#if (PY_MAJOR_VERSION == 2)

char *PyUnicode_AsUTF8(PyObject *unicode) {
    PyObject* str = PyUnicode_AsUTF8String(unicode);
    return PyString_AsString(str);
}
#endif
