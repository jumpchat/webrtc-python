#ifndef WEBRTC_MEDIASTREAMTRACK_H
#define WEBRTC_MEDIASTREAMTRACK_H

#include "Common.h"
#include "VideoRenderer.h"

namespace WebRTC {
class MediaStreamTrack {
    friend class MediaStream;

public:
    static MediaStreamTrack* New(webrtc::VideoTrackInterface* track);
    static MediaStreamTrack* New(webrtc::AudioTrackInterface* track);
    MediaStreamTrack();
    ~MediaStreamTrack();

    bool GetEnabled() { return _track->enabled(); }
    void SetEnabled(bool enabled) { _track->set_enabled(enabled); }
    std::string Id() { return _track->id(); }
    std::string Kind() { return _track->kind(); }
    std::string State();

    bool IsAudioTrack() { return _audio; }
    bool IsVideoTrack() { return _video; }

    void AddRenderer(VideoRenderer *renderer);
    void RemoveRenderer(VideoRenderer *renderer);

private:
    bool _audio;
    bool _video;
    rtc::scoped_refptr<webrtc::MediaStreamTrackInterface> _track;
    VideoRenderer* _renderer;
};
}

#endif // WEBRTC_MEDIASTREAMTRACK_H
