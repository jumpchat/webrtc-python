#ifndef WEBRTC_GETSOURCES_H
#define WEBRTC_GETSOURCES_H

#include "Common.h"
#include "MediaConstraints.h"

namespace WebRTC {

class GetSources {
public:
    static rtc::scoped_refptr<webrtc::AudioTrackInterface> GetAudioSource(const rtc::scoped_refptr<MediaConstraints>& constraints);
    static rtc::scoped_refptr<webrtc::AudioTrackInterface> GetAudioSource(const std::string id, const rtc::scoped_refptr<MediaConstraints>& constraints);

    static rtc::scoped_refptr<webrtc::VideoTrackInterface> GetVideoSource(const rtc::scoped_refptr<MediaConstraints>& constraints);
    static rtc::scoped_refptr<webrtc::VideoTrackInterface> GetVideoSource(const std::string id, const rtc::scoped_refptr<MediaConstraints>& constraints);

    static PyObject* GetDevices();
};

}

#endif
