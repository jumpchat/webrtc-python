#include "SessionDescription.h"

using namespace WebRTC;

SessionDescription::SessionDescription(const std::string &type, const std::string &sdp) {
    _sdp = webrtc::CreateSessionDescription(type, sdp, NULL);
}

SessionDescription::SessionDescription(webrtc::SessionDescriptionInterface *sdp) {
    _sdp = sdp;
}

SessionDescription::~SessionDescription() {

}

std::string SessionDescription::type() {
    return _sdp->type();
}

std::string SessionDescription::sdp() {
    std::string retval;
    _sdp->ToString(&retval);
    return retval;
}

