#include "Common.h"
#include "MediaStreamTrack.h"

using namespace WebRTC;

MediaStreamTrack* MediaStreamTrack::New(webrtc::VideoTrackInterface* track)
{
    MediaStreamTrack *self = new MediaStreamTrack();
    self->_track = track;
    self->_audio = false;
    self->_video = true;
    return self;
}

MediaStreamTrack* MediaStreamTrack::New(webrtc::AudioTrackInterface* track)
{
    MediaStreamTrack *self = new MediaStreamTrack();
    self->_track = track;
    self->_audio = true;
    self->_video = false;
    return self;
}

MediaStreamTrack::MediaStreamTrack()
    : _audio(false)
    , _video(false)
    , _renderer(NULL)
{
    LOG(LS_INFO) << __PRETTY_FUNCTION__;
}

MediaStreamTrack::~MediaStreamTrack()
{
    LOG(LS_INFO) << __PRETTY_FUNCTION__;
    _renderer = NULL;
}

std::string MediaStreamTrack::State() {
    LOG(LS_INFO) << __PRETTY_FUNCTION__;
    webrtc::MediaStreamTrackInterface::TrackState state;
    state = _track->state();
    switch (state) {
        case webrtc::MediaStreamTrackInterface::kLive:
            return "live";
        case webrtc::MediaStreamTrackInterface::kEnded:
            return "ended";
        default:
            return "invalid";
    }
}

void MediaStreamTrack::AddRenderer(VideoRenderer *renderer) {
    LOG(LS_INFO) << __PRETTY_FUNCTION__;
    _renderer = renderer;
    rtc::scoped_refptr<webrtc::VideoTrackInterface> track =
        reinterpret_cast<webrtc::VideoTrackInterface *>(_track.get());
    renderer->SetTrack(track);
    track->AddOrUpdateSink(renderer, rtc::VideoSinkWants());
}

void MediaStreamTrack::RemoveRenderer(VideoRenderer *renderer) {
    LOG(LS_INFO) << __PRETTY_FUNCTION__;
    _renderer = NULL;
    rtc::scoped_refptr<webrtc::VideoTrackInterface> track =
        reinterpret_cast<webrtc::VideoTrackInterface *>(_track.get());
    track->RemoveSink(renderer);
}
