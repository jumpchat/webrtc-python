#include "Common.h"
#include "MediaConstraints.h"

using namespace WebRTC;

MediaConstraints::MediaConstraints()
    : _audio(false)
    , _video(false)
{
    LOG(LS_INFO) << __PRETTY_FUNCTION__;
}

MediaConstraints::~MediaConstraints()
{
    LOG(LS_INFO) << __PRETTY_FUNCTION__;
}

rtc::scoped_refptr<MediaConstraints> MediaConstraints::New()
{
    LOG(LS_INFO) << __PRETTY_FUNCTION__;

    return new rtc::RefCountedObject<MediaConstraints>();
}

rtc::scoped_refptr<MediaConstraints> MediaConstraints::New(PyObject *constraints)
{
    LOG(LS_INFO) << __PRETTY_FUNCTION__;

    rtc::scoped_refptr<MediaConstraints> self = MediaConstraints::New();

    if (!constraints || !PyDict_Check(constraints)) {
        return self;
    }

    PyObject *optional_value = PyDict_GetItemString(constraints, "optional");
    if (optional_value && PyList_Check(optional_value)) {

        for (Py_ssize_t index = 0; index < PyList_Size(optional_value); index++) {
            PyObject *option_value = PyList_GetItem(optional_value, index);

            if (option_value && PyDict_Check(option_value)) {
                PyObject *DtlsSrtpKeyAgreement = PyDict_GetItemString(option_value, "DtlsSrtpKeyAgreement");
                PyObject *RtpDataChannels = PyDict_GetItemString(option_value, "RtpDataChannels");
                PyObject *googDscp = PyDict_GetItemString(option_value, "googDscp");
                PyObject *googIPv6 = PyDict_GetItemString(option_value, "googIPv6");
                PyObject *googSuspendBelowMinBitrate = PyDict_GetItemString(option_value, "googSuspendBelowMinBitrate");
                PyObject *googCombinedAudioVideoBwe = PyDict_GetItemString(option_value, "googCombinedAudioVideoBwe");
                PyObject *googScreencastMinBitrate = PyDict_GetItemString(option_value, "googScreencastMinBitrate");
                PyObject *googCpuOveruseDetection = PyDict_GetItemString(option_value, "googCpuOveruseDetection");
                PyObject *googPayloadPadding = PyDict_GetItemString(option_value, "googPayloadPadding");

                self->SetOptional(webrtc::MediaConstraintsInterface::kEnableDtlsSrtp, PyUnicode_AsUTF8(DtlsSrtpKeyAgreement));
                self->SetOptional(webrtc::MediaConstraintsInterface::kEnableRtpDataChannels, PyUnicode_AsUTF8(RtpDataChannels));
                self->SetOptional(webrtc::MediaConstraintsInterface::kEnableDscp, PyUnicode_AsUTF8(googDscp));
                self->SetOptional(webrtc::MediaConstraintsInterface::kEnableIPv6, PyUnicode_AsUTF8(googIPv6));
                self->SetOptional(webrtc::MediaConstraintsInterface::kEnableVideoSuspendBelowMinBitrate, PyUnicode_AsUTF8(googSuspendBelowMinBitrate));
                self->SetOptional(webrtc::MediaConstraintsInterface::kCombinedAudioVideoBwe, PyUnicode_AsUTF8(googCombinedAudioVideoBwe));
                self->SetOptional(webrtc::MediaConstraintsInterface::kScreencastMinBitrate, PyUnicode_AsUTF8(googScreencastMinBitrate));
                self->SetOptional(webrtc::MediaConstraintsInterface::kCpuOveruseDetection, PyUnicode_AsUTF8(googCpuOveruseDetection));
                self->SetOptional(webrtc::MediaConstraintsInterface::kPayloadPadding, PyUnicode_AsUTF8(googPayloadPadding));
            }
        }
    }

    PyObject *mandatory_value = PyDict_GetItemString(constraints, "mandatory");
    if (mandatory_value && PyDict_Check(mandatory_value)) {
        PyObject *OfferToReceiveAudio = PyDict_GetItemString(mandatory_value, "OfferToReceiveAudio");
        PyObject *OfferToReceiveVideo = PyDict_GetItemString(mandatory_value, "OfferToReceiveVideo");
        PyObject *VoiceActivityDetection = PyDict_GetItemString(mandatory_value, "VoiceActivityDetection");
        PyObject *IceRestart = PyDict_GetItemString(mandatory_value, "IceRestart");
        PyObject *googUseRtpMUX = PyDict_GetItemString(mandatory_value, "googUseRtpMUX");

        self->SetMandatory(webrtc::MediaConstraintsInterface::kOfferToReceiveAudio, OfferToReceiveAudio == Py_True);
        self->SetMandatory(webrtc::MediaConstraintsInterface::kOfferToReceiveVideo, OfferToReceiveVideo == Py_True);
        self->SetMandatory(webrtc::MediaConstraintsInterface::kVoiceActivityDetection, VoiceActivityDetection == Py_True);
        self->SetMandatory(webrtc::MediaConstraintsInterface::kIceRestart, IceRestart == Py_True);
        self->SetMandatory(webrtc::MediaConstraintsInterface::kUseRtpMux, googUseRtpMUX == Py_True);
    }

    PyObject *audio_value = PyDict_GetItemString(constraints, "audio");
    if (audio_value && PyBool_Check(audio_value)) {
        self->_audio = (audio_value == Py_True);
    } else if (audio_value && PyDict_Check(audio_value)) {
        PyObject *optional_value = PyDict_GetItemString(audio_value, "optional");

        if (optional_value && PyList_Check(optional_value)) {
            for (Py_ssize_t index = 0; index < PyList_Size(optional_value); index++) {
                PyObject *option = PyList_GetItem(optional_value, index);

                if (option && PyDict_Check(option)) {
                    PyObject *EchoCancellation = PyDict_GetItemString(option, "echoCancellation");
                    PyObject *googEchoCancellation = PyDict_GetItemString(option, "googEchoCancellation");
                    PyObject *googEchoCancellation2 = PyDict_GetItemString(option, "googEchoCancellation2");
                    PyObject *googDAEchoCancellation = PyDict_GetItemString(option, "googDAEchoCancellation");
                    PyObject *googAutoGainControl = PyDict_GetItemString(option, "googAutoGainControl");
                    PyObject *googAutoGainControl2 = PyDict_GetItemString(option, "googAutoGainControl2");
                    PyObject *googNoiseSuppression = PyDict_GetItemString(option, "googNoiseSuppression");
                    PyObject *googNoiseSuppression2 = PyDict_GetItemString(option, "googNoiseSuppression2");
                    PyObject *googHighpassFilter = PyDict_GetItemString(option, "googHighpassFilter");
                    PyObject *googTypingNoiseDetection = PyDict_GetItemString(option, "googTypingNoiseDetection");
                    PyObject *googAudioMirroring = PyDict_GetItemString(option, "googAudioMirroring");
                    PyObject *noiseReduction = PyDict_GetItemString(option, "googNoiseReduction");
                    PyObject *sourceId = PyDict_GetItemString(option, "sourceId");

                    self->SetOptional(webrtc::MediaConstraintsInterface::kEchoCancellation, PyUnicode_AsUTF8(EchoCancellation));
                    self->SetOptional(webrtc::MediaConstraintsInterface::kGoogEchoCancellation, PyUnicode_AsUTF8(googEchoCancellation));
                    self->SetOptional(webrtc::MediaConstraintsInterface::kExtendedFilterEchoCancellation, PyUnicode_AsUTF8(googEchoCancellation2));
                    self->SetOptional(webrtc::MediaConstraintsInterface::kDAEchoCancellation, PyUnicode_AsUTF8(googDAEchoCancellation));
                    self->SetOptional(webrtc::MediaConstraintsInterface::kAutoGainControl, PyUnicode_AsUTF8(googAutoGainControl));
                    self->SetOptional(webrtc::MediaConstraintsInterface::kExperimentalAutoGainControl, PyUnicode_AsUTF8(googAutoGainControl2));
                    self->SetOptional(webrtc::MediaConstraintsInterface::kNoiseSuppression, PyUnicode_AsUTF8(googNoiseSuppression));
                    self->SetOptional(webrtc::MediaConstraintsInterface::kExperimentalNoiseSuppression, PyUnicode_AsUTF8(googNoiseSuppression2));
                    self->SetOptional(webrtc::MediaConstraintsInterface::kHighpassFilter, PyUnicode_AsUTF8(googHighpassFilter));
                    self->SetOptional(webrtc::MediaConstraintsInterface::kTypingNoiseDetection, PyUnicode_AsUTF8(googTypingNoiseDetection));
                    self->SetOptional(webrtc::MediaConstraintsInterface::kAudioMirroring, PyUnicode_AsUTF8(googAudioMirroring));
                    self->SetOptional(webrtc::MediaConstraintsInterface::kNoiseReduction, PyUnicode_AsUTF8(noiseReduction));

                    if (sourceId && PyUnicode_Check(sourceId)) {
                        self->_audioId = PyUnicode_AsUTF8(sourceId);
                    }
                }
            }
        }
    }

    PyObject *video_value = PyDict_GetItemString(constraints, "video");
    if (video_value && PyBool_Check(video_value)) {
        self->_video = (audio_value == Py_True);
    } else if (video_value && PyDict_Check(video_value)) {
        PyObject *optional_value = PyDict_GetItemString(video_value, "optional");

        if (optional_value && PyList_Check(optional_value)) {
            for (Py_ssize_t index = 0; index < PyList_Size(optional_value); index++) {
                PyObject *option = PyList_GetItem(optional_value, index);

                if (option && PyDict_Check(option)) {

                    PyObject *minAspectRatio = PyDict_GetItemString(option, "minAspectRatio");
                    PyObject *maxAspectRatio = PyDict_GetItemString(option, "maxAspectRatio");
                    PyObject *maxWidth = PyDict_GetItemString(option, "maxWidth");
                    PyObject *minWidth = PyDict_GetItemString(option, "minWidth");
                    PyObject *maxHeight = PyDict_GetItemString(option, "maxHeight");
                    PyObject *minHeight = PyDict_GetItemString(option, "minHeight");
                    PyObject *maxFrameRate = PyDict_GetItemString(option, "maxFrameRate");
                    PyObject *minFrameRate = PyDict_GetItemString(option, "minFrameRate");
                    PyObject *sourceId = PyDict_GetItemString(option, "sourceId");

                    self->SetOptional(webrtc::MediaConstraintsInterface::kMinAspectRatio, PyUnicode_AsUTF8(minAspectRatio));
                    self->SetOptional(webrtc::MediaConstraintsInterface::kMaxAspectRatio, PyUnicode_AsUTF8(maxAspectRatio));
                    self->SetOptional(webrtc::MediaConstraintsInterface::kMaxWidth, PyUnicode_AsUTF8(maxWidth));
                    self->SetOptional(webrtc::MediaConstraintsInterface::kMinWidth, PyUnicode_AsUTF8(minWidth));
                    self->SetOptional(webrtc::MediaConstraintsInterface::kMaxHeight, PyUnicode_AsUTF8(maxHeight));
                    self->SetOptional(webrtc::MediaConstraintsInterface::kMinHeight, PyUnicode_AsUTF8(minHeight));
                    self->SetOptional(webrtc::MediaConstraintsInterface::kMaxFrameRate, PyUnicode_AsUTF8(maxFrameRate));
                    self->SetOptional(webrtc::MediaConstraintsInterface::kMinFrameRate, PyUnicode_AsUTF8(minFrameRate));

                    if (sourceId && PyUnicode_Check(sourceId)) {
                        self->_videoId = PyUnicode_AsUTF8(sourceId);
                    }
                }
            }
        }

        self->_video = true;
    }

    if (!self->GetOptional("RtpDataChannels")) {
        if (!self->IsOptional("DtlsSrtpKeyAgreement")) {
            self->SetOptional("DtlsSrtpKeyAgreement", "true");
        }
    }

    return self;
}

bool MediaConstraints::IsMandatory(const std::string& key)
{
    LOG(LS_INFO) << __PRETTY_FUNCTION__;

    std::string value;

    if (_mandatory.FindFirst(key, &value)) {
        return true;
    }

    return false;
}

bool MediaConstraints::GetMandatory(const std::string& key)
{
    LOG(LS_INFO) << __PRETTY_FUNCTION__;

    std::string value;

    if (_mandatory.FindFirst(key, &value)) {
        if (!value.compare("true")) {
            return true;
        }
    }

    return false;
}

void MediaConstraints::RemoveMandatory(const std::string& key)
{
    LOG(LS_INFO) << __PRETTY_FUNCTION__;

    std::string value;

    if (_mandatory.FindFirst(key, &value)) {
        for (webrtc::MediaConstraintsInterface::Constraints::iterator iter = _mandatory.begin(); iter != _mandatory.end(); ++iter) {
            if (iter->key == key) {
                _mandatory.erase(iter);
                break;
            }
        }
    }
}

void MediaConstraints::AddMandatory(const std::string& key, const std::string& value)
{
    LOG(LS_INFO) << __PRETTY_FUNCTION__;

    _mandatory.push_back(webrtc::MediaConstraintsInterface::Constraint(key, value));
}

void MediaConstraints::SetMandatory(const std::string& key, const std::string& value)
{
    LOG(LS_INFO) << __PRETTY_FUNCTION__;

    MediaConstraints::RemoveMandatory(key);
    MediaConstraints::AddMandatory(key, value);
}

bool MediaConstraints::IsOptional(const std::string& key)
{
    LOG(LS_INFO) << __PRETTY_FUNCTION__;

    std::string value;

    if (_mandatory.FindFirst(key, &value)) {
        return true;
    }

    return false;
}

bool MediaConstraints::GetOptional(const std::string& key)
{
    LOG(LS_INFO) << __PRETTY_FUNCTION__;

    std::string value;

    if (_optional.FindFirst(key, &value)) {
        if (!value.compare("true")) {
            return true;
        }
    }

    return false;
}

void MediaConstraints::RemoveOptional(const std::string& key)
{
    LOG(LS_INFO) << __PRETTY_FUNCTION__;

    std::string value;

    if (_optional.FindFirst(key, &value)) {
        for (webrtc::MediaConstraintsInterface::Constraints::iterator iter = _optional.begin(); iter != _optional.end(); ++iter) {
            if (iter->key == key) {
                _optional.erase(iter);
                break;
            }
        }
    }
}


void MediaConstraints::AddOptional(const std::string& key, const std::string& value)
{
    LOG(LS_INFO) << __PRETTY_FUNCTION__;

    _optional.push_back(webrtc::MediaConstraintsInterface::Constraint(key, value));
}

void MediaConstraints::SetOptional(const std::string& key, const std::string& value)
{
    LOG(LS_INFO) << __PRETTY_FUNCTION__;

    MediaConstraints::RemoveOptional(key);
    MediaConstraints::AddOptional(key, value);
}

bool MediaConstraints::UseAudio() const
{
    LOG(LS_INFO) << __PRETTY_FUNCTION__;

    return _audio;
}

bool MediaConstraints::UseVideo() const
{
    LOG(LS_INFO) << __PRETTY_FUNCTION__;

    return _video;
}

std::string MediaConstraints::AudioId() const
{
    LOG(LS_INFO) << __PRETTY_FUNCTION__;

    return _audioId;
}

std::string MediaConstraints::VideoId() const
{
    LOG(LS_INFO) << __PRETTY_FUNCTION__;

    return _videoId;
}

const webrtc::MediaConstraintsInterface* MediaConstraints::ToConstraints() const
{
    LOG(LS_INFO) << __PRETTY_FUNCTION__;

    return this;
}

const webrtc::MediaConstraintsInterface::Constraints& MediaConstraints::GetMandatory() const
{
    LOG(LS_INFO) << __PRETTY_FUNCTION__;

    return _mandatory;
}

const webrtc::MediaConstraintsInterface::Constraints& MediaConstraints::GetOptional() const
{
    LOG(LS_INFO) << __PRETTY_FUNCTION__;

    return _optional;
}