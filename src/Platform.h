#ifndef WEBRTC_PLATFORM_H
#define WEBRTC_PLATFORM_H

#include "Common.h"
#include <string>

namespace WebRTC {
    class Platform {
        public:
            static void Init();
            static void Dispose();
            static void SetDebug(const std::string &id);

            static rtc::scoped_refptr<webrtc::PeerConnectionFactoryInterface>GetFactory();

        private:
            static rtc::scoped_refptr<webrtc::PeerConnectionFactoryInterface> _factory;
            static rtc::Thread _signal_thread;
            static rtc::Thread _worker_thread;
            static rtc::Thread _network_thread;
    };
}

#endif // WEBRTC_PLATFORM_H
