#ifndef __WEBRTC_VIDEORENDERER_H__
#define __WEBRTC_VIDEORENDERER_H__

#include "Common.h"

namespace WebRTC {

class VideoRenderer : public rtc::VideoSinkInterface<webrtc::VideoFrame> {
    public:
        VideoRenderer();
        ~VideoRenderer();

        void SetOnFrame(PyObject *callback);
        PyObject *GetOnFrame();

        // callback from webrtc
        void OnFrame(const webrtc::VideoFrame& frame) final;
        static int OnFrameCb(void *arg);

        void SetTrack(webrtc::VideoTrackInterface *track);
        void SetEventHandler(PyObject *event_handler) { _event_handler = event_handler; }
        void CallMethod(PyObject* obj, const char* method_name, ...);

    private:
        rtc::scoped_refptr<webrtc::I420Buffer> _frame;
        rtc::scoped_refptr<webrtc::VideoTrackInterface> _track;
        PyObject *_callback;
        PyObject *_event_handler;
};

}

#endif // __WEBRTC_VIDEORENDERER_H__
