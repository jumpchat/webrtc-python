import os
import unittest

import webrtc

class TestGetUserMedia(unittest.TestCase):
    def test_getUserMedia(self):
        this = self
        def successCallback(stream):
            print("successCallback: {}".format(stream.label))
            this.localStream = stream

        def errorCallback(error):
            print("errorCallback: {}".format(error))

        webrtc.getUserMedia({
            'video': True,
            'audio': True
        },
        successCallback,
        errorCallback)

        self.assertIsNotNone(self.localStream)

        tracks = self.localStream.getVideoTracks()
        self.assertGreaterEqual(len(tracks), 1)

    def tearDown(self):
        webrtc.dispose()

if __name__ == '__main__':
    unittest.main()