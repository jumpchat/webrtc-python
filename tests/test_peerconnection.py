import os
import unittest
import time
import webrtc

webrtc.setDebug("verbose")

class TestPeerConnection(unittest.TestCase):

    def createPC(self):
        pc = webrtc.PeerConnection({
                'iceServers': [{
                        'url': 'stun:stun.l.google.com:19302'
                }],
            },
            {
                'audio': True,
                'video': True
            })

        def oniceconnectionstatechange(self, state):
           print("oniceconnectionstatechange", self, state)
        pc.oniceconnectionstatechange = pc.bind(oniceconnectionstatechange)

        def onsignalingstatechange(self, state):
           print("onsignalingstatechange", self, state)
        pc.onsignalingstatechange = pc.bind(onsignalingstatechange)

        return pc

    def createStream(self):
        def successCallback(stream):
            print("successCallback: {}".format(stream.label))
            self._localStream = stream

        def errorCallback(error):
            print("errorCallback: {}".format(error))

        self._localStream = None
        webrtc.getUserMedia({
            'video': True,
            'audio': True
        },
        successCallback,
        errorCallback)

        while (not self._localStream):
            self.sleep()
        return self._localStream

    def sleep(self):
        time.sleep(0.01)
        # pass

    def test_create(self):
        pc = self.createPC()
        self.assertIsNotNone(pc)

        f = pc.createOffer()
        localSDP = f.result()
        self.assertIsNotNone(localSDP)

        f = pc.setLocalDescription(localSDP)
        self.assertTrue(f.result())

    def test_remote(self):
        pc1 = self.createPC()
        pc2 = self.createPC()

        localStream = self.createStream()
        pc1.addStream(localStream)

        f = pc1.createOffer()
        self.pc1sdp = f.result()
        self.assertIsNotNone(self.pc1sdp)
        f = pc1.setLocalDescription(self.pc1sdp)
        # should call this to get the result
        f.result()
        self.assertTrue(f.result())
        f = pc2.setRemoteDescription(self.pc1sdp)
        # should call this to get the result
        f.result()
        self.assertTrue(f.result())

        f = pc2.createAnswer()
        self.pc2sdp = f.result()
        self.assertIsNotNone(self.pc2sdp)
        f = pc2.setLocalDescription(self.pc2sdp)
        # should call this to get the result
        f.result()
        f = pc1.setRemoteDescription(self.pc2sdp)
        # should call this to get the result
        f.result()

        pc1.removeStream(localStream)

        pc1.close()
        pc2.close()

    def tearDown(self):
        webrtc.dispose()

if __name__ == '__main__':
    unittest.main()
