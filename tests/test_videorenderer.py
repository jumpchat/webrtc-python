import os
import unittest
import time
import webrtc
import png

webrtc.setDebug('verbose')

def saveFrame(frame):
    # Save PNG
    width = frame['width']
    height = frame['height']
    y = frame['y']
    print(len(y))
    y2d = []
    for i in range(height):
        y2d.append(y[i*width:(i+1)*width])
    f = open('frame.png', 'wb')
    w = png.Writer(width, height, greyscale=True)
    w.write(f, y2d)
    f.close()

class TestVideoRenderer(unittest.TestCase):
    def test_videoRenderer(self):

        this = self
        def successCallback(stream):
            print("successCallback: {}".format(stream.label))
            this.localStream = stream

        def errorCallback(error):
            print("errorCallback: {}".format(error))

        webrtc.getUserMedia({
            'video': True,
            'audio': True
        },
        successCallback,
        errorCallback)

        self.assertIsNotNone(self.localStream)

        def onFrame(self, frame):
            print('{}x{}: {} {} {}'.format(frame['width'], frame['height'],
                len(frame['y']), len(frame['u']), len(frame['v'])))
            self.frameCount += 1
            saveFrame(frame)

        renderer = webrtc.VideoRenderer()
        renderer.frameCount = 0
        renderer.onframe = renderer.bind(onFrame)

        tracks = self.localStream.getVideoTracks()
        self.assertGreaterEqual(len(tracks), 1)

        tracks[0].addRenderer(renderer)

        webrtc.processEvents(timeout=3)

        print('frameCount', renderer.frameCount)
        self.assertGreaterEqual(renderer.frameCount, 1)

        tracks[0].removeRenderer(renderer)

    def tearDown(self):
        webrtc.dispose()

if __name__ == '__main__':
    unittest.main()