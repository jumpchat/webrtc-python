import os
import unittest

import webrtc

class TestGetSources(unittest.TestCase):
    def test_getSources(self):
        sources = webrtc.getSources()
        print(sources)
        self.assertGreaterEqual(len(sources), 1)

    def tearDown(self):
        webrtc.dispose()

if __name__ == '__main__':
    unittest.main()