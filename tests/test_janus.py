import os
import unittest

import webrtc
import janus
import time
import webrtc
import types
import png

# webrtc log
# webrtc.setDebug('verbose')

# janus log
# import logging
# logging.getLogger('janus').setLevel(logging.DEBUG)
# logging.getLogger('janus').addHandler(logging.StreamHandler())

g_roomid = 9000

class VideoRoomPlugin(janus.Plugin):
    name = 'janus.plugin.videoroom'
    options = {
        'bitrate': 128000,
        'username': 'webrtc-python',
        'roomid': g_roomid
    }
    def __init__(self, test):
        janus.Plugin.__init__(self)
        self.on_message.connect(self.onMessage)
        self.pc = webrtc.PeerConnection(config={
            'iceServers': [{"url": "stun:stun.l.google.com:19302"}]
        })
        this = self

        def onaddstream(self, stream):
            print("onaddstream", self, stream)
            test.stream = stream
        self.pc.onaddstream = self.pc.bind(onaddstream)

        def onremovestream(self, stream):
            print('onremovestream', stream)
        self.pc.onremovestream = self.pc.bind(onremovestream)

        def onsignalchange(self, state):
            print('onsignalchange', state)
        self.pc.onsignalchange = self.pc.bind(onsignalchange)

        def onicecandidate(self, candidate):
            print('candidate', candidate)
            message = {
                'janus': 'trickle',
                'candidate': candidate
            }
            return this.send_message(message)
        self.pc.onicecandidate = self.pc.bind(onicecandidate)

        def oniceconnectionstatechange(self, state):
            print('oniceconnectionstatechange', state)
        self.pc.oniceconnectionstatechange = self.pc.bind(oniceconnectionstatechange)

        def onicegatheringstatechange(self, state):
            print('onicegatheringstatechange', state)
        self.pc.onicegatheringstatechange = self.pc.bind(onicegatheringstatechange)

        def onnegotiationneeded(self):
            print('onnegotiationneeded')
        self.pc.onnegotiationneeded = self.pc.bind(onnegotiationneeded)

    def listRooms(self):
        request = { 'request': 'list' }
        return self.send_message(request)

    def roomExists(self, roomid):
        request = { "request": "exists", "room": roomid }
        return self.send_message(request)

    def join(self, roomid):
        request = {
            "request": "join",
            "room": roomid,
            "ptype": "publisher",
            "bitrate": self.options['bitrate'],
            "display": self.options['username']
        }
        return self.send_message(request)

    def leave(self):
        request = { "request": "leave" }
        return self.send_message(request)


    def publish(self, offer):
        publish = {
            "request": "publish",
            "audio": True,
            "video": True
        }
        request = {
            'janus': 'message',
            'body': publish,
            'jsep': offer
        }

        return self.send_message(request)


    def listUsers(self, roomid):
        request = { "request": "listparticipants", "room": roomid }
        return self.send_message(request)


    def listen(self, roomid, userid):
        request = {
            "request": "join",
            "room": roomid,
            "ptype": "listener",
            "feed": userid,
            "audio": True,
            "video": True
        }
        return self.send_message(request)

    def start(self, roomid, sdp):
        body = { "request": "start", "room": roomid }
        self.send_message({"janus": "message", "body": body, "jsep": sdp})

    def onMessage(self, *args, **kwargs):
        message = kwargs.get('message')
        print('message', message)
        sdp = kwargs.get('sdp')
        if sdp is not None:
            # print('on_message', sdp)
            self.pc.setRemoteDescription(sdp).result()
            f = self.pc.createAnswer({
				'mandatory': {
					'OfferToReceiveAudio': True,
					'OfferToReceiveVideo': True,
				}
            })
            self.pc.setLocalDescription(f.result()).result()
            self.start(self.options['roomid'], f.result())

def saveFrame(frame):
    try:
        width = frame['width']
        height = frame['height']
        y = frame['y']
        y2d = []
        for i in range(height):
            y2d.append(y[i*width:(i+1)*width])
        f = open('frame.png', 'wb+')
        w = png.Writer(width, height, greyscale=True)
        w.write(f, y2d)
        f.close()
        print("save frame.png")
    except:
        print('bad frame', frame)

class TestJanus(unittest.TestCase):
    def test_janus(self):
        self.stream = None

        janus_ws_url = 'wss://sd6.dcpfs.net:8989/janus'
        session = janus.Session(janus_ws_url)
        videoroom = VideoRoomPlugin(self)

        # def on_message(videoroom, message=None, sdp=None):
        #     if sdp is not None:
        #         print sdp
        # videoroom.on_message.connect(on_message)


        session.register_plugin(videoroom)
        f = session.connect()
        self.assertTrue(f.result(10) is session)
        self.assertTrue(session.is_connected)
        self.assertTrue(len(session.plugins) == 1)

        f = session.create()
        self.assertTrue(f.result(10) is session)
        self.assertTrue(session.is_connected)
        self.assertTrue(len(session.plugins) == 1)

        f = videoroom.listRooms()
        result = f.result()
        self.assertTrue(len(result['list']) > 1)

        # roomInfo = videoroom.join(9000)
        # result = roomInfo.result()
        # print result
        # self.assertEqual(result['videoroom'], 'joined')
        # self.assertEqual(result['room'], 9000)

        f = videoroom.listUsers(g_roomid)
        participants = f.result()['participants']
        self.assertGreater(len(participants), 0)
        user =  participants[0]

        result = videoroom.listen(g_roomid, user['id'])

        for i in range(5):
            if (self.stream):
                break
            # time.sleep(1)
            webrtc.processEvents(1)
        self.assertIsNotNone(self.stream)

        print('stream', self.stream)

        def onFrame(self, frame):
            # print('{}x{}: {} {} {}'.format(frame['width'], frame['height'],
            #     len(frame['y']), len(frame['u']), len(frame['v'])))
            saveFrame(frame)
            self.frameCount += 1

        renderer = webrtc.VideoRenderer()
        renderer.frameCount = 0
        renderer.onframe = renderer.bind(onFrame)

        tracks = self.stream.getVideoTracks()
        self.assertGreaterEqual(len(tracks), 1)

        tracks[0].addRenderer(renderer)

        for i in range(50):
            webrtc.processEvents()

        print("done")
        self.assertGreaterEqual(renderer.frameCount, 1)

    def tearDown(self):
        webrtc.dispose()


if __name__ == '__main__':
    unittest.main()