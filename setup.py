#!/usr/bin/env python

"""
setup.py file for SWIG example
"""

from distutils.core import setup, Extension

import buildwebrtc
import platform
import os

buildwebrtc.download_build()

c_files = [
    'src/webrtc.i',
    'src/BackTrace.cpp',
    'src/Platform.cpp',
    'src/GetUserMedia.cpp',
    'src/GetSources.cpp',
    'src/Observer.cpp',
    'src/PeerConnection.cpp',
    'src/MediaConstraints.cpp',
    'src/MediaStream.cpp',
    'src/MediaStreamTrack.cpp',
    'src/SessionDescription.cpp',
    'src/Utils.cpp',
    'src/VideoRenderer.cpp',
]
libraries = ['webrtc']
include_dirs = [buildwebrtc.WEBRTC_INCLUDE]
library_dirs = [buildwebrtc.WEBRTC_LIB_DIR]
extra_link_args = []
extra_compile_args = []

platform_system = platform.system()
if platform_system == 'Linux':
    define_macros = [
        ('_GLIBCXX_USE_CXX11_ABI', '0'),
        ('WEBRTC_LINUX', None),
        ('WEBRTC_POSIX', '1'),
        ('NDEBUG', None),
        ('SWIG_PYTHON_THREADS', None),
    ]
    libraries += ['X11']
    extra_compile_args += [
        '-std=c++11',
        '-fpermissive'
    ]
elif platform_system == 'Darwin':
    define_macros = [
        ('WEBRTC_MAC', None),
        ('WEBRTC_IOS', None),
        ('WEBRTC_POSIX', '1'),
        ('NDEBUG', None),
        ('SWIG_PYTHON_THREADS', None),
    ]
    extra_link_args += [
        '-framework', 'Foundation',
        '-framework', 'CoreAudio',
        '-framework', 'CoreMedia',
        '-framework', 'CoreVideo',
        '-framework', 'CoreGraphics',
        '-framework', 'AudioToolbox',
        '-framework', 'AVFoundation',
    ]
    extra_compile_args += [
        '-std=c++11',
    ]
elif platform_system == 'Windows':
    define_macros = [
        ('WEBRTC_WIN', None),
        ('NOGDI', None),
        ('NOMINMAX', None),
        ('NDEBUG', None),
        ('SWIG_PYTHON_THREADS', None),
    ]
    libraries += [
        'dmoguids',
        'msdmo',
        'secur32',
        'winmm',
        'wmcodecdspuuid',
        'ws2_32',
        'dmoguids',
        'amstrmid',
        'advapi32'
    ]
    extra_compile_args += [
        "/MT",
        "/EHsc"
    ]

webrtc_module = Extension('_webrtc',
                          sources=c_files,
                          libraries=libraries,
                          define_macros=define_macros,
                          include_dirs=include_dirs,
                          library_dirs=library_dirs,
                          extra_compile_args=extra_compile_args,
                          extra_link_args=extra_link_args,
                          swig_opts=['-c++']
                          )

setup(name='webrtc',
      version='0.0.1',
      author="JumpChat",
      description="""WebRTC bindings for Python""",
      license='LGPL',
      ext_modules=[webrtc_module],
      py_modules=["webrtc"],
      install_requires=[
        'blinker >=1.4,<2',
        'futures >=3.0.3,<4',
        'ws4py >=0.3.4,<0.4',
      ],
    )
