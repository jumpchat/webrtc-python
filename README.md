WebRTC Python Bindings
======================

Python bindings for webrtc.

How to build
------------

```
# sh is needed to build webrtc library
% pip install sh
% python setup.py build
```

Running tests
-------------

```
% python run_tests.py
```

Install package
---------------

```
% python setup.py install
```

Mac
---

Enable debug log
```
% defaults write org.python.python logToStdErr true
```
