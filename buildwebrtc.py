import os
import sys
import re
import platform
import fnmatch
from subprocess import check_call
import six

# constants
CONFIG = 'Release'
ARCH = 'x64'
if platform.machine() == 'armv7l':
    ARCH = 'arm'

platform_system = platform.system()
if platform_system == 'Linux':
    WEBRTC_OS = 'linux'
elif platform_system == 'Darwin':
    WEBRTC_OS = 'mac'
elif platform_system == 'Windows':
    WEBRTC_OS = 'win'

WEBRTC_VERSION = '1.0.1'
WEBRTC_REVISION = '0dabfbfd895add37ea367017777b9adbb43f9313'
WEBRTC_ROOT_DIR = os.path.join(os.environ['HOME'], '.webrtc')
WEBRTC_BUILD_DIR = os.path.join(WEBRTC_ROOT_DIR, 'webrtc', WEBRTC_REVISION)
DEPOT_TOOLS_REPO = 'https://chromium.googlesource.com/chromium/tools/depot_tools.git'
DEPOT_TOOLS_DIR = os.path.join(WEBRTC_ROOT_DIR, 'depot_tools')
WEBRTC_DIR = os.path.join(WEBRTC_ROOT_DIR, 'webrtc', WEBRTC_REVISION)
WEBRTC_REPO = 'https://chromium.googlesource.com/external/webrtc.git'
WEBRTC_SYNC_FILE = os.path.join(WEBRTC_BUILD_DIR, 'webrtc_sync')
WEBRTC_SRC = os.path.join(WEBRTC_BUILD_DIR, 'src')
WEBRTC_INCLUDE = os.path.join(WEBRTC_BUILD_DIR, 'include')
WEBRTC_OUT = os.path.join(WEBRTC_SRC, WEBRTC_OS, ARCH, 'out', CONFIG)
WEBRTC_BUILD_ARGS = ' '.join([
    '--args=is_component_build=false',
    'rtc_include_tests=false',
    'is_debug={}'.format(str(CONFIG == 'Debug').lower()),
    'target_cpu="{}"'.format(ARCH),
    'target_os="{}"'.format(WEBRTC_OS)
])
WEBRTC_LIB_DIR =  os.path.join(WEBRTC_BUILD_DIR, 'lib', WEBRTC_OS, ARCH)
if platform_system == 'Windows':
    WEBRTC_LIB = os.path.join(WEBRTC_LIB_DIR, 'webrtc.lib')
else:
    WEBRTC_LIB = os.path.join(WEBRTC_LIB_DIR, 'libwebrtc.a')


# add depot tools to path
os.environ['PATH'] = os.environ['PATH'] + ':' + DEPOT_TOOLS_DIR

def clone_depot_tools():
    print(' * Syncing depot_tools')
    if not os.path.exists(WEBRTC_ROOT_DIR):
        os.makedirs(WEBRTC_ROOT_DIR)

    if os.path.exists(DEPOT_TOOLS_DIR):
        check_call("git pull", cwd=DEPOT_TOOLS_DIR, shell=True)
    else:
        check_call("git clone {}".format(DEPOT_TOOLS_REPO), cwd=WEBRTC_ROOT_DIR, shell=True)

def gclient_config():
    if not os.path.exists(WEBRTC_BUILD_DIR):
        os.makedirs(WEBRTC_BUILD_DIR)
    print(' * Configuring gclient')
    check_call('gclient config --unmanaged --name=src {}'.format(WEBRTC_REPO),
        cwd=WEBRTC_BUILD_DIR, shell=True)

def gclient_sync():
    print(' * Syncing webrtc sources (this may take a long time...)')
    if os.path.exists(WEBRTC_SYNC_FILE):
        return

    check_call('gclient sync --revision {} --reset --no-history --delete_unversioned_trees'.format(WEBRTC_REVISION),
        cwd=WEBRTC_BUILD_DIR, shell=True)

    # touch sync file if succeeds
    f = open(WEBRTC_SYNC_FILE, 'w')
    f.close()

def gn_gen():
    print(' * Generating build files')
    check_call("gn gen {} '{}'".format(WEBRTC_OUT, WEBRTC_BUILD_ARGS),
        cwd=WEBRTC_SRC, shell=True)

def ninja_build():
    print(' * Compiling webrtc')
    check_call('ninja -C {} examples'.format(WEBRTC_OUT),
        cwd=WEBRTC_SRC, shell=True)

def create_lib():
    print(' * Create static library')
    if not os.path.exists(WEBRTC_LIB_DIR):
        os.makedirs(WEBRTC_LIB_DIR)
    path = os.path.join(WEBRTC_OUT, 'obj')

    files = []
    for root, dirnames, filenames in os.walk(path):
        for filename in fnmatch.filter(filenames, '*.o'):
            if not re.match(r'.*(examples|protobuf_full|protoc|yasm/gen|yasm/re2c|yasm/yasm)', filename):
                files.append(os.path.join(root, filename))

    platform_system = platform.system()
    file_list = os.path.join(WEBRTC_OUT, 'libwebrtc_objs.txt')
    with open(file_list, 'w') as fl:
        for f in files:
            fl.write(f + '\n')

    if platform_system == 'Linux':
        file_list = os.path.join(WEBRTC_OUT, 'libwebrtc_objs.txt')
        with open(file_list, 'w') as fl:
            fl.write("CREATE {}\n".format(WEBRTC_LIB))
            for f in files:
                fl.write("ADDMOD {}\n".format(f))
            fl.write("SAVE\n")
        check_call('ar -M < {}'.format(file_list),
            shell=True)
        check_call('ranlib {}'.format(WEBRTC_LIB),
            shell=True)
    elif platform_system == 'Darwin':
        check_call('libtool -static -o {} -no_warning_for_no_symbols -filelist {}'.format(WEBRTC_LIB, file_list),
            shell=True)
    elif platform_system == 'Windows':
        check_call('lib /out:{} @{}'.format(WEBRTC_LIB, file_list),
            shell=True)

def build():
    if os.path.exists(WEBRTC_LIB):
        return

    clone_depot_tools()
    gclient_config()
    gclient_sync()
    gn_gen()
    ninja_build()
    create_lib()

def download_build():
    if os.path.exists(WEBRTC_LIB):
        return

    filename = 'libwebrtc-{}-{}-{}.tar.xz'.format(WEBRTC_OS, ARCH, WEBRTC_VERSION)
    url = 'https://bitbucket.org/jumpchat/webrtc-lib/downloads/{}'.format(filename)

    if not os.path.exists(filename):
        print(" * Downloading {}".format(url))
        open(filename, 'wb').write(six.moves.urllib.request.urlopen(url).read())

    if not os.path.exists(WEBRTC_ROOT_DIR):
        os.makedirs(WEBRTC_ROOT_DIR)

    print(" * Unpacking {}".format(url))
    check_call('tar -xf {} -C {}'.format(filename, WEBRTC_ROOT_DIR), shell=True)

if __name__ == '__main__':
   build()