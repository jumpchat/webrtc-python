import _webrtc
from concurrent.futures import Future
from threading import Thread
from six.moves.queue import Queue, Empty
import types
# from _webrtc import PeerConnection

_webrtc.Platform_Init()

class EventQueue():

    def __init__(self):
        self.frequency = 2.0
        self.queue = Queue()

    def put(self, item):
        self.queue.put(item)

    def process(self, timeout=1):
        try:
            while True:
                item = self.queue.get(timeout=timeout)
                # print("item: {}".format(item))
                obj = item[0]
                method = item[1]
                args = item[2]
                # print("calling method {}.{}({})".format(obj, method, args))
                obj_method = getattr(obj, method)
                if obj_method is not None:
                    obj_method(*args)
        except Empty:
            return False
        # except Exception as e:
        #     print("callback {} error: {} {}".format(method, e.__class__, e))

event_queue = EventQueue()



_webrtc.Platform_Init()

# timeout in seconds
def processEvents(timeout=1):
    event_queue.process(timeout=timeout)

def setDebug(val):
    _webrtc.Platform_SetDebug(val)

def dispose():
    _webrtc.Platform_Dispose()

def getUserMedia(constraints, successCallback=None, errorCallback=None):

    def successCb(stream):
        if stream and successCallback:
            mediaStream = MediaStream(stream)
            successCallback(mediaStream)

    def errorCb(error):
        if error and errorCallback:
            errorCallback(error)

    _webrtc.GetUserMedia_GetMediaStream(constraints, successCb, errorCb)

def getSources():
    return _webrtc.GetSources_GetDevices()

class MediaStreamTrack(object):
    def __init__(self, other=None):
        if other:
            self.this = other
        else:
            self.this = _webrtc.new_MediaStreamTrack()

    __swig_destroy__ = _webrtc.delete_MediaStreamTrack
    __del__ = lambda self: None

    def __getattr__(self, name):
        if name == 'id':
            return _webrtc.MediaStreamTrack_Id(self.this)
        elif name == 'enabled':
            return _webrtc.MediaStreamTrack_GetEnabled(self.this)
        elif name == 'kind':
            return _webrtc.MediaStreamTrack_Kind(self.this)
        elif name == 'state':
            return _webrtc.MediaStreamTrack_State(self.this)
        elif name == 'audio':
            return _webrtc.MediaStreamTrack_IsAudioTrack(self.this)
        elif name == 'video':
            return _webrtc.MediaStreamTrack_IsVideoTrack(self.this)

    def __setattr__(self, name, value):
        if name == 'enabled':
            _webrtc.MediaStreamTrack_SetEnabled(self.this, value)
        else:
            super(MediaStreamTrack, self).__setattr__(name, value)

    def addRenderer(self, renderer):
        if type(renderer) is VideoRenderer:
            _webrtc.MediaStreamTrack_AddRenderer(self, renderer.this)
        else:
            raise TypeError(renderer)

    def removeRenderer(self, renderer):
        if type(renderer) is VideoRenderer:
            _webrtc.MediaStreamTrack_RemoveRenderer(self, renderer.this)
        else:
            raise TypeError(renderer)
_webrtc.MediaStreamTrack_swigregister(MediaStreamTrack)

class MediaStream(object):
    def __init__(self, other=None):
        if other:
            self.this = other
        else:
            self.this = _webrtc.new_MediaStream()

    __swig_destroy__ = _webrtc.delete_MediaStream
    __del__ = lambda self: None

    def __getattr__(self, name):
        if name == 'label':
            return _webrtc.MediaStream_label(self.this)

    def getAudioTracks(self):
        tracks = _webrtc.MediaStream_GetAudioTracks(self.this)
        return tracks

    def getVideoTracks(self):
        tracks = _webrtc.MediaStream_GetVideoTracks(self.this)
        retval = [MediaStreamTrack(t) for t in tracks]
        return retval
_webrtc.MediaStream_swigregister(MediaStream)



class SessionDescription(object):
    def __init__(self, type, sdp):
        self.this = _webrtc.new_SessionDescription(type, sdp)
    __swig_destroy__ = _webrtc.delete_SessionDescription
    __del__ = lambda self: None

    def __getattr__(self, name):
        if name == 'type':
            return _webrtc.SessionDescription_type(self.this)
        elif name == 'sdp':
            return _webrtc.SessionDescription_sdp(self.this)
        else:
            return super(VideoRenderer, self).__getattr__(name)
_webrtc.SessionDescription_swigregister(SessionDescription)

class PeerConnection(object):

    def __init__(self, config=None, constraints=None):
        self.this = _webrtc.new_PeerConnection(config, constraints)
        _webrtc.PeerConnection_SetObserver(self.this, self, event_queue)
    __swig_destroy__ = _webrtc.delete_PeerConnection
    __del__ = lambda self: None

    def addIceCandidate(self, candidate):
        _webrtc.PeerConnection_AddIceCanddaite(self.this, candidate)

    def addStream(self, stream):
        _webrtc.PeerConnection_AddStream(self.this, stream.this)

    def removeStream(self, stream):
        _webrtc.PeerConnection_RemoveStream(self, stream.this)

    def addTrack(self, track, stream):
        pass

    def removeTrack(self, track):
        pass

    def createOffer(self, *args, **kwargs):
        f = Future()
        numargs = len(args)
        if (numargs == 0):
            _webrtc.PeerConnection_CreateOffer(self.this, {}, f)
        elif (numargs == 1):
            _webrtc.PeerConnection_CreateOffer(self.this, args[0], f)
        return f

    def createAnswer(self, *args, **kwargs):
        f = Future()
        numargs = len(args)
        if (numargs == 0):
            _webrtc.PeerConnection_CreateAnswer(self.this, {}, f)
        elif (numargs == 1):
            _webrtc.PeerConnection_CreateAnswer(self.this, args[0], f)
        return f

    def setLocalDescription(self, *args, **kwargs):
        f = Future()
        numargs = len(args)
        if (numargs == 1):
            _webrtc.PeerConnection_SetLocalSessionDescription(self.this, args[0], f)
        return f

    def setRemoteDescription(self, *args, **kwargs):
        f = Future()
        numargs = len(args)
        if (numargs == 1):
            _webrtc.PeerConnection_SetRemoteSessionDescription(self.this, args[0], f)
        return f

    def getLocalStream():
        return self.local_streams

    def getRemoteStream():
        return self.remote_streams

    def close(self):
        _webrtc.PeerConnection_Close(self.this)

    def bind(self, callback):
        return types.MethodType( callback, self )

    def __getattr__(self, name):
        if name == 'local_streams':
            return None
        elif name == 'remote_streams':
            return None
        elif name == 'signaling_state':
            return None
        elif name == 'ice_connection_state':
            return None
        elif name == 'ice_gathering_state':
            return None
        else:
            return None
_webrtc.PeerConnection_swigregister(PeerConnection)


class VideoRenderer(object):

    def __init__(self):
        self.this = _webrtc.new_VideoRenderer()
        _webrtc.VideoRenderer_SetEventHandler(self.this, event_queue)
        _webrtc.VideoRenderer_SetOnFrame(self.this, self)
    __swig_destroy__ = _webrtc.delete_VideoRenderer
    __del__ = lambda self: None

    # def __getattr__(self, name):
    #     if name == 'onframe':
    #         return _webrtc.VideoRenderer_GetOnFrame(self.this)
    #     else:
    #         return super(VideoRenderer, self).__getattr__(name)

    # def __setattr__(self, name, value):
    #     if name == 'onframe':
    #         _webrtc.VideoRenderer_SetOnFrame(self.this, value)
    #     else:
    #         super(VideoRenderer, self).__setattr__(name, value)

    def bind(self, callback):
        return types.MethodType( callback, self )

_webrtc.VideoRenderer_swigregister(VideoRenderer)
